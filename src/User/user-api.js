import {HOST} from '../API/hosts';
import RestApiClient from "../API/rest-client";

const endpoint = {
    createUserPath: '/user/create',
    logInUserPath: '/user/logIn'
};

function postUser(userDTOBody, callback){
    let request = new Request(HOST.backend_api + endpoint.createUserPath , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(userDTOBody)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function logInUser(userDTOBody, callback){
    let request = new Request(HOST.backend_api + endpoint.logInUserPath , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(userDTOBody)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export{
    postUser,
    logInUser
};
