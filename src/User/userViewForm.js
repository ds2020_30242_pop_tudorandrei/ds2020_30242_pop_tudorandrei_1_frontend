import React from 'react';
import { FormGroup} from 'reactstrap';
import { Input, Label} from 'reactstrap';
import UserControllerApi from '../build/openapi/src/api/UserControllerApi';
import {withRouter} from 'react-router-dom';



class UserViewForm extends React.Component{


    constructor(props){

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        
        this.formatDate = this.formatDate.bind(this);

        this.state = {
            errorStatus : 0,
            error : null,
            formIsValid : false,
            user : this.props.user,

            formControls:{
                firstName:{
                    value : this.props.user.userFirstName,
                    valid : true,
                    touched : false,
                    validationRules:{
                        minLength : 3,
                        isRequired: true
                    }
                },
                lastName:{
                    value : this.props.user.userLastName,
                    valid : true,
                    touched : false,
                    validationRules:{
                        minLength : 3,
                        isRequired: true
                    }
                },
                userName:{
                    value : this.props.user.userName,
                    valid : true,
                    touched : false,
                    validationRules:{
                        minLength : 6,
                        isRequired: true
                    }
                },
                password:{
                    value : this.props.user.userPassword,
                    valid : true,
                    touched : false,
                    validationRules:{
                        minLength : 6,
                        isRequired: true
                    }
                },
                address:{
                    value : this.props.user.userAddress,
                    valid : true,
                    touched : false,
                    validationRules:{
                        minLength : 6
                    }
                },
                dateOfBirth:{
                    value : this.formatDate(this.props.user.userBirthDate),
                    valid : true,
                    touched : false,
                    validationRules:{
                        dateValidator: true,
                        isRequired: true
                    }
                },
                gender:{
                    value : this.props.user.userGender,
                    valid : true,
                    touched : false,
                    validationRules:{
                        isRequired: true
                    }
                },
            }
        };
    }

    //https://stackoverflow.com/questions/23593052/format-javascript-date-as-yyyy-mm-dd
    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [day, month, year].join('/');
    }

    toggleForm(){
        this.setState({collapseForm: !this.state.collapseForm});
    }


    render(){
        console.log(this.state.user);
        return (
            
            <div>

                <FormGroup id='firstName'>
                    <Label for='firstNameField'> FirstName: </Label>
                    <Input name='firstName' id='firstNameField' placeholder = {this.state.formControls.firstName.placeholder}
                           defaultValue = {this.state.formControls.firstName.value}
                           touched={this.state.formControls.firstName.touched ? 1 : 0}
                           valid = {this.state.formControls.firstName.valid}
                           disabled
                    />
                </FormGroup>

                <FormGroup id='lastName'>
                    <Label for='lastNameField'> LastName: </Label>
                    <Input name='lastName' id='lastNameField' placeholder = {this.state.formControls.lastName.placeholder}
                           defaultValue = {this.state.formControls.lastName.value}
                           touched={this.state.formControls.lastName.touched ? 1 : 0}
                           valid = {this.state.formControls.lastName.valid}
                           disabled
                    />
                </FormGroup>

                <FormGroup id='userName'>
                    <Label for='userNameField'> UserName: </Label>
                    <Input name='userName' id='userNameField' placeholder = {this.state.formControls.userName.placeholder}
                           defaultValue = {this.state.formControls.userName.value}
                           touched={this.state.formControls.userName.touched ? 1 : 0}
                           valid = {this.state.formControls.userName.valid}
                           disabled
                    />
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' placeholder = {this.state.formControls.password.placeholder}
                           defaultValue = {this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched ? 1 : 0}
                           valid = {this.state.formControls.password.valid}
                           disabled
                    />
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> address: </Label>
                    <Input name='address' id='addressField' placeholder = {this.state.formControls.address.placeholder}
                           defaultValue = {this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched ? 1 : 0}
                           valid = {this.state.formControls.address.valid}
                           disabled
                    />
                </FormGroup>

                <FormGroup id='dateOfBirth'>
                    <Label for='dateOfBirthField'> Date Of Birth: </Label>
                    <Input name='dateOfBirth' id='dateOfBirthField' placeholder = {this.state.formControls.dateOfBirth.placeholder}
                           defaultValue = {this.state.formControls.dateOfBirth.value}
                           touched={this.state.formControls.dateOfBirth.touched ? 1 : 0}
                           valid = {this.state.formControls.dateOfBirth.valid}
                           disabled
                    />
                </FormGroup>

                <FormGroup id='gender'>
                    <Label for='genderField'> gender: </Label>
                    <Input name='gender' id='genderField' 
                           defaultValue = {this.state.formControls.gender.value}
                           touched={this.state.formControls.gender.touched ? 1 : 0}
                           valid = {this.state.formControls.gender.valid}
                           disabled
                    />
                </FormGroup>
            </div>
        );

    }
}

export default withRouter(UserViewForm);