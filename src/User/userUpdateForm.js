import React from 'react';
import Button from 'react-bootstrap/Button';
import {Col, FormGroup, Row} from 'reactstrap';
import {Form, Input, Label} from 'reactstrap';
import UserControllerApi from '../build/openapi/src/api/UserControllerApi';
import User from '../build/openapi/src/model/User';
import validate from './register-validator';
import Select from 'react-select';
import {withRouter} from 'react-router-dom';
import * as PatientAPI from "../patient/patient-api"
import * as CaregiverAPI from "../caregiver/caregiver-api"


const genderOptions = [
    { value: 'MALE', label: 'Male' },
    { value: 'FEMALE', label: 'Female' },
    { value: 'Lol', label: 'Other' }
  ];

class UpdateForm extends React.Component{


    constructor(props){

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        //this.reloadHandler = this.props.reloadHandler; // nu cred ca nevoie de asa ceva aici
        this.userControllerApi = new UserControllerApi();
        
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeModal = props.closeModal.bind(this);
        this.formatDate = this.formatDate.bind(this);

        this.state = {
            errorStatus : 0,
            error : null,
            formIsValid : false,
            user : this.props.user,

            formControls:{
                firstName:{
                    value : this.props.user.userFirstName,
                    valid : true,
                    touched : false,
                    validationRules:{
                        minLength : 3,
                        isRequired: true
                    }
                },
                lastName:{
                    value : this.props.user.userLastName,
                    valid : true,
                    touched : false,
                    validationRules:{
                        minLength : 3,
                        isRequired: true
                    }
                },
                userName:{
                    value : this.props.user.userName,
                    valid : true,
                    touched : false,
                    validationRules:{
                        minLength : 6,
                        isRequired: true
                    }
                },
                password:{
                    value : this.props.user.userPassword,
                    valid : true,
                    touched : false,
                    validationRules:{
                        minLength : 6,
                        isRequired: true
                    }
                },
                address:{
                    value : this.props.user.userAddress,
                    valid : true,
                    touched : false,
                    validationRules:{
                        minLength : 6
                    }
                },
                dateOfBirth:{
                    value : this.formatDate(this.props.user.userBirthDate),
                    valid : true,
                    touched : false,
                    validationRules:{
                        dateValidator: true,
                        isRequired: true
                    }
                },
                gender:{
                    value : this.props.user.userGender,
                    valid : true,
                    touched : false,
                    validationRules:{
                        isRequired: true
                    }
                },
            }
        };
    }

    //https://stackoverflow.com/questions/23593052/format-javascript-date-as-yyyy-mm-dd
    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [day, month, year].join('/');
    }

    toggleForm(){
        this.setState({collapseForm: !this.state.collapseForm});
    }

    handleChange = event =>{
        const name = event.target.name;
        const value = event.target.value;
        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules); // needs a validator
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for(let updatedFormElement in updatedControls){
            formIsValid = updatedControls[updatedFormElement].valid && formIsValid;
        }

        this.setState({
            formControls : updatedControls,
            formIsValid : formIsValid
        });

    };

    handleSelectChange = (option, action) =>{          // <Select> nu returneaza un event object ci (optinuea alea, [ceva are seamana cu event])
        const name = action.name;
        const value = option.value;                    // optiunea aleasa (value, label)
        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        console.log(value);

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules); // needs a validator
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for(let updatedFormElement in updatedControls){
            formIsValid = updatedControls[updatedFormElement].valid && formIsValid;
        }

        this.setState({
            formControls : updatedControls,
            formIsValid : formIsValid
        });

    };

    handleSubmit(){
        let updatedUser = new User();

        
        updatedUser.userId = this.state.user.userId;
        updatedUser.userFirstName = this.state.formControls.firstName.value ;
        updatedUser.userLastName = this.state.formControls.lastName.value;
        updatedUser.userAddress = this.state.formControls.address.value;
        updatedUser.userBirthDate = this.state.formControls.dateOfBirth.value;
        updatedUser.userGender = this.state.formControls.gender.value;

        updatedUser.userName = this.state.formControls.userName.value;
        updatedUser.userPassword = this.state.formControls.password.value;
        updatedUser.userRole = this.state.user.userRole;

        console.log(JSON.stringify(updatedUser));
        this.updateUser(updatedUser);

    }

    updateUser(updatedUser){
        this.closeModal();
        switch (updatedUser.userRole) {
            case 'PATIENT':
                PatientAPI.updatePatient(updatedUser, (result,status, error) =>{
                    if(status == 200 ||status == 201){
                        //ok
                        
                    }else{
                        this.setState(({
                            errorStatus : status,
                            error : error
                        }));
                    }
                });
                break;
                case 'CAREGIVER':
                    CaregiverAPI.updateCaregiver(updatedUser, (result,status, error) =>{
                        if(status == 200 ||status == 201){
                            //ok
                            
                        }else{
                            this.setState(({
                                errorStatus : status,
                                error : error
                            }));
                        }
                    });
                    break;
                
        
            default:
                break;
        }
        
    }

    render(){

        return (
            <div>

                <FormGroup id='firstName'>
                    <Label for='firstNameField'> FirstName: </Label>
                    <Input name='firstName' id='firstNameField' placeholder = {this.state.formControls.firstName.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.firstName.value}
                           touched={this.state.formControls.firstName.touched ? 1 : 0}
                           valid = {this.state.formControls.firstName.valid}
                           required
                    />
                    {
                        this.state.formControls.firstName.touched &&
                         !this.state.formControls.firstName.valid &&
                         <div className="error-message row"> * First name must have at least 3 characters </div>
                    }
                </FormGroup>

                <FormGroup id='lastName'>
                    <Label for='lastNameField'> LastName: </Label>
                    <Input name='lastName' id='lastNameField' placeholder = {this.state.formControls.lastName.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.lastName.value}
                           touched={this.state.formControls.lastName.touched ? 1 : 0}
                           valid = {this.state.formControls.lastName.valid}
                           required
                    />
                    {
                        this.state.formControls.lastName.touched &&
                         !this.state.formControls.lastName.valid &&
                         <div className="error-message row"> * Last name must have at least 3 characters </div>
                    }
                </FormGroup>

                <FormGroup id='userName'>
                    <Label for='userNameField'> UserName: </Label>
                    <Input name='userName' id='userNameField' placeholder = {this.state.formControls.userName.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.userName.value}
                           touched={this.state.formControls.userName.touched ? 1 : 0}
                           valid = {this.state.formControls.userName.valid}
                           required
                    />
                    {
                        this.state.formControls.userName.touched &&
                         !this.state.formControls.userName.valid &&
                         <div className="error-message row"> * UserName must have at least 3 characters </div>
                    }
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' placeholder = {this.state.formControls.password.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched ? 1 : 0}
                           valid = {this.state.formControls.password.valid}
                           required
                           password
                    />
                    {
                        this.state.formControls.password.touched &&
                         !this.state.formControls.password.valid &&
                         <div className="error-message row"> * Password must have at least 6 characters </div>
                    }
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> address: </Label>
                    <Input name='address' id='addressField' placeholder = {this.state.formControls.address.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched ? 1 : 0}
                           valid = {this.state.formControls.address.valid}
                           required
                    />
                    {
                        this.state.formControls.address.touched &&
                         !this.state.formControls.address.valid &&
                         <div className="error-message row"> * Address must have at least 6 characters </div>
                    }
                </FormGroup>

                <FormGroup id='dateOfBirth'>
                    <Label for='dateOfBirthField'> Date Of Birth: </Label>
                    <Input name='dateOfBirth' id='dateOfBirthField' placeholder = {this.state.formControls.dateOfBirth.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.dateOfBirth.value}
                           touched={this.state.formControls.dateOfBirth.touched ? 1 : 0}
                           valid = {this.state.formControls.dateOfBirth.valid}
                           required
                    />
                    {
                        this.state.formControls.dateOfBirth.touched &&
                         !this.state.formControls.dateOfBirth.valid &&
                         <div className="error-message row"> * First name must have at least 3 characters </div>
                    }
                </FormGroup>

                <FormGroup id='gender'>
                    <Label for='genderField'> Select gender: </Label>
                    <Select name='gender' options = {genderOptions} id='genderField' placeholder = {this.state.formControls.gender.placeholder}
                         onChange = {this.handleSelectChange}
                         defaultValue = {this.state.formControls.gender.value}
                         touched={this.state.formControls.gender.touched ? 1 : 0}
                         valid = {this.state.formControls.gender.valid}
                         required            
                    />
                    {
                        this.state.formControls.gender.touched &&
                         !this.state.formControls.gender.valid &&
                         <div className="error-message row">You must select a gender</div>
                    }
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>
            </div>
        );

    }
}

export default withRouter(UpdateForm);