import React from 'react';
import Button from 'react-bootstrap/Button';
import {Col, FormGroup, Row} from 'reactstrap';
import {Form, Input, Label} from 'reactstrap';
import UserControllerApi from '../build/openapi/src/api/UserControllerApi';
import validate from './register-validator';
import LogInEntity from '../build/openapi/src/model/LogInEntity';
import * as UserAPI from "./user-api";
import { withRouter } from 'react-router-dom';


const REGISTER_PATH = '/register';


class UserLogInForm extends React.Component{

    constructor(props){
        

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.history = this.props.history;

        //this.logOutHandler = this.props.whenLogOut; // nu cred ca nevoie de asa ceva aici
        this.logInHandler = this.props.whenLogIn;
        this.logInErrorHandler = this.props.whenLogInError;

        this.userControllerApi = new UserControllerApi();
        
        this.handleChange = this.handleChange.bind(this);
        this.handleLogInSubmit = this.handleLogInSubmit.bind(this);


        this.state = {
            errorStatus : 0,
            error : null,
            formIsValid : false,

            formControls:{
                username:{
                    value : '',
                    placeholder: 'Enter your username',
                    valid : false,
                    touched : false,
                    validationRules:{
                        minLength : 6,
                        isRequired: true
                    }
                },
                password:{
                    value : '',
                    placeholder: 'Enter your password',
                    valid : false,
                    touched : false,
                    validationRules:{
                        minLength : 6,
                        isRequired: true
                    }
                },
            }
 
        };
        console.log(props);
    }

    toggleForm(){
        this.setState({collapseForm: !this.state.collapseForm});
    }

    handleChange = event =>{
        const name = event.target.name;
        const value = event.target.value;
        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules); // needs a validator
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for(let updatedFormElement in updatedControls){
            formIsValid = updatedControls[updatedFormElement].valid && formIsValid;
        }

        this.setState({
            formControls : updatedControls,
            formIsValid : formIsValid
        });

    };

    handleRegisterUser(){
        this.history.push('/register');
    }
    // Daca dau asa la register crapa, dau daca dau lambda merge, sau o lamba care o apeleaza ???????????????/ wtf 

   
    handleLogInSubmit(){
        let logInDetails = new LogInEntity();

        logInDetails.username = this.state.formControls.username.value;
        logInDetails.password = this.state.formControls.password.value;

        //just for now
        console.log(JSON.stringify(logInDetails));
        this.accountLogIn(logInDetails);

    }


    accountLogIn(logInDetails){

        try{
            UserAPI.logInUser(logInDetails, (result, status, error) =>{
                if(status == 201 || status == 200 || status == 202){

                    this.logInHandler(result.userId, result.userRole); // cals the logInHandler form 'caller'
                    //push history here ?
                    // this.pushUserPath(result.userRole); /// wait, i works without lambda 'onClick'??? what ?
                    
                }else{
                    this.setState(({
                        errorStatus : status,
                        error : error
                    }));

                    this.logInErrorHandler();  // cals the logInErrorHandler form 'caller'
                }
        });
        }catch(err){
            this.logInErrorHandler();
        }
        
    }

    render(){

        return (
            <div>

                <FormGroup id='username'>
                    <Label for='usernameField'> Username: </Label>
                    <Input name='username' id='usernameField' placeholder = {this.state.formControls.username.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.username.value}
                           touched={this.state.formControls.username.touched ? 1 : 0}
                           valid = {this.state.formControls.username.valid}
                           required
                    />
                    {
                        this.state.formControls.username.touched &&
                         !this.state.formControls.username.valid &&
                         <div className="error-message row"> * username </div>
                    }
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' placeholder = {this.state.formControls.password.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched ? 1 : 0}
                           valid = {this.state.formControls.password.valid}
                           required
                    />
                    {
                        this.state.formControls.password.touched &&
                         !this.state.formControls.password.valid &&
                         <div className="error-message row"> * password </div>
                    }
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleLogInSubmit}>  Log in </Button>
                    </Col>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} onClick={() => {this.handleRegisterUser()}}>  Register </Button>
                    </Col>
                </Row>
            </div>
        );

    }
}

export default withRouter(UserLogInForm);