import React from 'react';
import Button from 'react-bootstrap/Button';
import {Col, FormGroup, Row} from 'reactstrap';
import {Form, Input, Label} from 'reactstrap';
import UserControllerApi from '../build/openapi/src/api/UserControllerApi';
import User from '../build/openapi/src/model/User';
import UserBodyDTOs from '../build/openapi/src/model/UserBodyDTOs';
import UserDetailsDTO from '../build/openapi/src/model/UserDetailsDTO';
import UserDTO from '../build/openapi/src/model/UserDTO';
import CreatableSelect from 'react-select/creatable';
import validate from './register-validator';
import Select from 'react-select';
import * as UserAPI from "./user-api";
import {withRouter} from 'react-router-dom';


const genderOptions = [
    { value: 'MALE', label: 'Male' },
    { value: 'FEMALE', label: 'Female' },
    { value: 'Lol', label: 'Other' }
  ];

  const roleOptions = [
    { value: 'PATIENT', label: 'Patient' },
    { value: 'CAREGIVER', label: 'Caregiver' },
    { value: 'DOCTOR', label: 'Doctor' }
  ];

  const LOGIN_PATH = '/login';

class RegisterForm extends React.Component{


    constructor(props){

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        //this.reloadHandler = this.props.reloadHandler; // nu cred ca nevoie de asa ceva aici
        this.userControllerApi = new UserControllerApi();
        
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            errorStatus : 0,
            error : null,
            formIsValid : false,

            formControls:{
                firstName:{
                    value : '',
                    placeholder: 'Enter your first name',
                    valid : false,
                    touched : false,
                    validationRules:{
                        minLength : 3,
                        isRequired: true
                    }
                },
                lastName:{
                    value : '',
                    placeholder: 'Enter your last name',
                    valid : false,
                    touched : false,
                    validationRules:{
                        minLength : 3,
                        isRequired: true
                    }
                },
                userName:{
                    value : '',
                    placeholder: 'Enter user name (used for log-in)',
                    valid : false,
                    touched : false,
                    validationRules:{
                        minLength : 6,
                        isRequired: true
                    }
                },
                password:{
                    value : '',
                    placeholder: 'password',
                    valid : false,
                    touched : false,
                    validationRules:{
                        minLength : 6,
                        isRequired: true
                    }
                },
                repeatPassword:{
                    value : '',
                    placeholder: 'Repreat password',
                    valid : false,
                    touched : false,
                    validationRules:{
                        minLength : 6,
                        isRequired: true
                    }
                },
                address:{
                    value : '',
                    placeholder: 'Enter your address',
                    valid : false,
                    touched : false,
                    validationRules:{
                        minLength : 6
                    }
                },
                dateOfBirth:{
                    value : '',
                    placeholder: 'Enter your birth date as dd/MM/yyyy',
                    valid : false,
                    touched : false,
                    validationRules:{
                        dateValidator: true,
                        isRequired: true
                    }
                },
                gender:{
                    value : '',
                    placeholder: 'Select your gender',
                    valid : false,
                    touched : false,
                    validationRules:{
                        isRequired: true
                    }
                },
                role:{
                    value : '',
                    placeholder: 'Select your role',
                    valid : false,
                    touched : false,
                    validationRules:{
                        isRequired: true
                    }
                },
            }
        };
    }

    toggleForm(){
        this.setState({collapseForm: !this.state.collapseForm});
    }

    handleChange = event =>{
        const name = event.target.name;
        const value = event.target.value;
        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules); // needs a validator
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for(let updatedFormElement in updatedControls){
            formIsValid = updatedControls[updatedFormElement].valid && formIsValid;
        }

        this.setState({
            formControls : updatedControls,
            formIsValid : formIsValid
        });

    };

    handleSelectChange = (option, action) =>{          // <Select> nu returneaza un event object ci (optinuea alea, [ceva are seamana cu event])
        const name = action.name;
        const value = option.value;                    // optiunea aleasa (value, label)
        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        console.log(value);

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules); // needs a validator
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for(let updatedFormElement in updatedControls){
            formIsValid = updatedControls[updatedFormElement].valid && formIsValid;
        }

        this.setState({
            formControls : updatedControls,
            formIsValid : formIsValid
        });

    };

    handleSubmit(){
        let userDetailDTO = new UserDetailsDTO();
        let userDTO = new UserDTO();
        
        let userBodyDTO = new UserBodyDTOs();

        userDetailDTO.id = null;
        userDetailDTO.firstName = this.state.formControls.firstName.value ;
        userDetailDTO.lastName = this.state.formControls.lastName.value;
        userDetailDTO.address = this.state.formControls.address.value;
        userDetailDTO.birthDate = this.state.formControls.dateOfBirth.value;
        userDetailDTO.gender = this.state.formControls.gender.value;

        userDTO.id = null;
        userDTO.userName = this.state.formControls.userName.value;
        userDTO.password = this.state.formControls.password.value;
        userDTO.role = this.state.formControls.role.value;

        userBodyDTO.userDTO = userDTO;
        userBodyDTO.userDetailsDTO = userDetailDTO;

        //just for now
        console.log(JSON.stringify(userBodyDTO));
        this.createUser(userBodyDTO);

    }

    createUser(userBodyDTO){
        UserAPI.postUser(userBodyDTO, (result, status, error) =>{
            console.log(result);
                if(status == 201 || status == 200){
                    //ok
                    this.props.history.push(LOGIN_PATH);

                }else{
                    this.setState(({
                        errorStatus : status,
                        error : error
                    }));
                }
        } );
    }

    render(){

        return (
            <div>

                <FormGroup id='firstName'>
                    <Label for='firstNameField'> FirstName: </Label>
                    <Input name='firstName' id='firstNameField' placeholder = {this.state.formControls.firstName.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.firstName.value}
                           touched={this.state.formControls.firstName.touched ? 1 : 0}
                           valid = {this.state.formControls.firstName.valid}
                           required
                    />
                    {
                        this.state.formControls.firstName.touched &&
                         !this.state.formControls.firstName.valid &&
                         <div className="error-message row"> * First name must have at least 3 characters </div>
                    }
                </FormGroup>

                <FormGroup id='lastName'>
                    <Label for='lastNameField'> LastName: </Label>
                    <Input name='lastName' id='lastNameField' placeholder = {this.state.formControls.lastName.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.lastName.value}
                           touched={this.state.formControls.lastName.touched ? 1 : 0}
                           valid = {this.state.formControls.lastName.valid}
                           required
                    />
                    {
                        this.state.formControls.lastName.touched &&
                         !this.state.formControls.lastName.valid &&
                         <div className="error-message row"> * Last name must have at least 3 characters </div>
                    }
                </FormGroup>

                <FormGroup id='userName'>
                    <Label for='userNameField'> UserName: </Label>
                    <Input name='userName' id='userNameField' placeholder = {this.state.formControls.userName.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.userName.value}
                           touched={this.state.formControls.userName.touched ? 1 : 0}
                           valid = {this.state.formControls.userName.valid}
                           required
                    />
                    {
                        this.state.formControls.userName.touched &&
                         !this.state.formControls.userName.valid &&
                         <div className="error-message row"> * UserName must have at least 3 characters </div>
                    }
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' placeholder = {this.state.formControls.password.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched ? 1 : 0}
                           valid = {this.state.formControls.password.valid}
                           required
                           password
                    />
                    {
                        this.state.formControls.password.touched &&
                         !this.state.formControls.password.valid &&
                         <div className="error-message row"> * Password must have at least 6 characters </div>
                    }
                </FormGroup>

                <FormGroup id='repeatPassword'>
                    <Label for='repeatPasswordField'> Repeat Password: </Label>
                    <Input name='repeatPassword' id='repeatPasswordField' placeholder = {this.state.formControls.repeatPassword.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.repeatPassword.value}
                           touched={this.state.formControls.repeatPassword.touched ? 1 : 0}
                           valid = {this.state.formControls.repeatPassword.valid}
                           required
                           password
                    />
                    {
                        this.state.formControls.repeatPassword.touched &&
                         !this.state.formControls.repeatPassword.valid &&
                         <div className="error-message row"> * Passwords must match </div>
                    }
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> address: </Label>
                    <Input name='address' id='addressField' placeholder = {this.state.formControls.address.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched ? 1 : 0}
                           valid = {this.state.formControls.address.valid}
                           required
                    />
                    {
                        this.state.formControls.address.touched &&
                         !this.state.formControls.address.valid &&
                         <div className="error-message row"> * Address must have at least 6 characters </div>
                    }
                </FormGroup>

                <FormGroup id='dateOfBirth'>
                    <Label for='dateOfBirthField'> Date Of Birth: </Label>
                    <Input name='dateOfBirth' id='dateOfBirthField' placeholder = {this.state.formControls.dateOfBirth.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.dateOfBirth.value}
                           touched={this.state.formControls.dateOfBirth.touched ? 1 : 0}
                           valid = {this.state.formControls.dateOfBirth.valid}
                           required
                    />
                    {
                        this.state.formControls.dateOfBirth.touched &&
                         !this.state.formControls.dateOfBirth.valid &&
                         <div className="error-message row"> * First name must have at least 3 characters </div>
                    }
                </FormGroup>

                <FormGroup id='gender'>
                    <Label for='genderField'> Select gender: </Label>
                    <Select name='gender' options = {genderOptions} id='genderField' placeholder = {this.state.formControls.gender.placeholder}
                         onChange = {this.handleSelectChange}
                         defaultValue = {this.state.formControls.gender.value}
                         touched={this.state.formControls.gender.touched ? 1 : 0}
                         valid = {this.state.formControls.gender.valid}
                         required            
                    />
                    {
                        this.state.formControls.gender.touched &&
                         !this.state.formControls.gender.valid &&
                         <div className="error-message row">You must select a gender</div>
                    }
                </FormGroup>

                <FormGroup id='role'>
                    <Label for='roleField'> Select role: </Label>
                    <Select name='role' options = {roleOptions} id='roleField' placeholder = {this.state.formControls.role.placeholder}
                         onChange = {this.handleSelectChange}
                         defaultValue = {this.state.formControls.role.value}
                         touched={this.state.formControls.role.touched ? 1 : 0}
                         valid = {this.state.formControls.role.valid}
                         required>
                            
                    </Select>
                    {
                        this.state.formControls.role.touched &&
                         !this.state.formControls.role.valid &&
                         <div className="error-message row"> * You must select a role </div>
                    }
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>

                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} onClick={() => {
                                this.props.history.push(LOGIN_PATH)}
                           }> I am a member </Button>
                    </Col>
                </Row>
            </div>
        );

    }
}

export default withRouter(RegisterForm);