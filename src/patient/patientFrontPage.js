import React from 'react'
import {withRouter} from 'react-router-dom';
import PatientNavBar from './PatientNavBar';
import PatientControllerApi from '../build/openapi/src/api/PatientControllerApi';
import PatientTable from '../doctor/doctor-patient/PatientTable';
import PrescribedMedicationForm from '../prescribedMedication/prescribedMedicationTable';
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import UserViewForm from '../User/userViewForm';

const patientControllerApi = new PatientControllerApi();

const medPlanColumns = [
    {
        Header: 'Id',
        accessor: 'medicationPlanId',
    },
    {
        Header: 'StartDate',
        accessor: 'startDate',
    },
    {
        Header: 'EndDate',
        accessor: 'endDate',
    },
    {
        Header: 'Medication',
        accessor: 'viewMedicationButton',
    }
];

class PatientFrontPage extends React.Component {

    
    constructor(props){
        
        super(props);
        this.patientId = props.componentId;
        this.toggleMedicine = this.toggleMedicine.bind(this);
        this.toggleInfo = this.toggleInfo.bind(this);

        this.state = {
            loggedIn : false,
            patient : JSON.parse(window.localStorage.getItem('user')),
            medicineSelected : false,
            userInfoSelected : false,
            currentUser : null,
            currentMedicationList : [],
            medicationPlans : [],
            medicationPlansLoaded : false,
        };
    }

    
    bindPatient(){

        if(this.state.patient === undefined || this.state.patient === null)
        {
            patientControllerApi.getPatientById(this.patientId, (err, data, resp)=>{
                this.setState({patient : data, loggedIn: true});
                window.localStorage.setItem('user', JSON.stringify(data, this.getCircularReplacer()));
                window.localStorage.setItem('userId', JSON.stringify(this.state.patient.patientId));
            });
        }else{
            this.setState({loggedIn: true});
        }
    }

    bindMedicationPlans(){

        if(this.patientId !== undefined ||this.patientId !== null )
        {
            patientControllerApi.getMedicationPlans(this.patientId, (err, data, resp)=>{
                this.setState({medicationPlans : data, medicationPlansLoaded: true});
            });
        }else{
            this.setState({loggedIn: true});
        }
    }


    componentDidMount(){
       this.bindPatient();
       this.bindMedicationPlans();
    }

    //https://stackoverflow.com/questions/23593052/format-javascript-date-as-yyyy-mm-dd
    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [day, month, year].join('/');
    }

    //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cyclic_object_value
    getCircularReplacer = () => {
        const seen = new WeakSet();
        return (key, value) => {
          if (typeof value === "object" && value !== null) {
            if (seen.has(value)) {
              return;
            }
            seen.add(value);
          }
          return value;
        };
      };

    toggleMedicine(prescribedMedications){
        this.setState({medicineSelected : ! this.state.medicineSelected, currentMedicationList : prescribedMedications});
    }

    toggleInfo(user){
        this.setState({userInfoSelected : ! this.state.userInfoSelected, currentUser : user});
    }

    render() {
        console.log(this.state.patient);
        

        if(this.state.loggedIn && this.state.medicationPlansLoaded){
            var medicationPlansCopy;    
            medicationPlansCopy = this.state.medicationPlans.slice(0);
            medicationPlansCopy.forEach(medPlan => {
                medPlan.startDate = this.formatDate(medPlan.startDate);
                medPlan.endDate = this.formatDate(medPlan.endDate);
                medPlan.viewMedicationButton = <Button onClick = {() => {this.toggleMedicine(medPlan.prescribedMedications)}}>Medicine</Button>
            });
        }

        return (
        <div>
        {this.state.loggedIn && <PatientNavBar handleInfo = {()=>{this.toggleInfo(this.state.patient.user)}} handleLogOut= {this.props.handleLogOut}/>}

            <CardHeader>
                <strong> Medication Plans </strong>
            </CardHeader>
            <Card>
                <Row>
                    <Col sm={{size: '8', offset: 1}}>
                            {this.state.medicationPlansLoaded && <PatientTable desiredColumns={medPlanColumns} tableData ={medicationPlansCopy}/>} 
                    </Col>
                </Row>
            </Card>

            <Modal isOpen={this.state.medicineSelected} toggle={this.toggleMedicine}
                    className={this.props.className} size="lg">
                <ModalHeader toggle={this.toggleMedicine}> MedicationModal: </ModalHeader>
                <ModalBody>
                    <PrescribedMedicationForm medicine ={this.state.currentMedicationList} ></PrescribedMedicationForm>
                </ModalBody>
            </Modal>

            <Modal isOpen={this.state.userInfoSelected} toggle={this.toggleInfo}
                    className={this.props.className} size="lg">
                <ModalHeader toggle={this.toggleInfo}> UserDetails: </ModalHeader>
                <ModalBody>
                    <h1>UserDetails</h1>
                    <UserViewForm user = {this.state.currentUser}></UserViewForm>
                </ModalBody>
            </Modal>

        </div>

        )
    };
}

export default withRouter(PatientFrontPage);
