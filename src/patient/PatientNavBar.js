import React from 'react'
import logo from '../commons/images/icon.png';

import {
    Nav,
    Navbar,
    NavbarBrand,
    Button
} from 'reactstrap';
import {withRouter} from 'react-router-dom';



class PatientNavBar extends React.Component{
    constructor(props){

        super(props);
        this.handleLogOut = props.handleLogOut.bind(this);
        this.handleInfo = props.handleInfo.bind(this);
    }


    render(){
        return (
            <div>
                <Navbar color="dark" light expand="md">
                    <NavbarBrand href="/patient">
                        <img src={logo} width={"50"}
                             height={"35"} />
                    </NavbarBrand>
                    <Nav className="mr-auto" navbar>
        
                        <div>
                            <Button color="primary" className = "col col-lg-2" onClick = {() =>{
                                        this.handleInfo();// togle user info modal
                                    }}>ViewUserInfo</Button>
                        </div>
        
                        <div>
                            <Button color="primary" className = "col col-lg-2" onClick = {() =>{
                                        this.props.history.push('/'); //change path
                                        this.handleLogOut(); // logout
                                    }}>LogOut</Button>
                        </div>
                    </Nav>
                </Navbar>
            </div>
        );
    }
}
export default withRouter(PatientNavBar);