import React from 'react'
import {withRouter} from 'react-router-dom';
import PatientTable from '../doctor/doctor-patient/PatientTable';
import {
    Card,
    CardHeader,
    Col,
    Row
} from 'reactstrap';


const medicineColumns = [
    {
        Header: 'DrugName',
        accessor: 'drug.drugName',
    },
    {
        Header: 'IntakeMoment',
        accessor: 'intakeMoment',
    },
    {
        Header: 'Dosage(mg)',
        accessor: 'dosage',
    },
    
    {
        Header: 'DrugSideEffects',
        accessor: 'drug.drugSideEffects',
    }
];

class PrescribedMedicationForm extends React.Component {

    
    constructor(props){
        
        super(props);
        this.state = {
            medicine : this.props.medicine
        };
    }

    render() {
        return (
        <div>
            <CardHeader>
                <strong> Medication Plans </strong>
            </CardHeader>
            <Card>
                <Row>
                    <Col sm={{size: '8', offset: 1}}>
                            {this.state.medicine !== undefined && <PatientTable desiredColumns={medicineColumns} tableData ={this.state.medicine}/>} 
                    </Col>
                </Row>
            </Card>
        </div>

        )
    };
}

export default withRouter(PrescribedMedicationForm);
