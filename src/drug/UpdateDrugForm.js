import React from 'react';
import Button from 'react-bootstrap/Button';
import {Col, FormGroup, Row} from 'reactstrap';
import {Input, Label} from 'reactstrap';
import validate from '../User/register-validator';
import * as DrugAPI from './drug-api';

import {withRouter} from 'react-router-dom';
import Drug from '../build/openapi/src/model/Drug';

class UpdateDrugForm extends React.Component{


    constructor(props){

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeModal = this.props.closeModal.bind(this);
        this.parentUpdate = this.props.parentUpdate.bind(this);

        this.state = {
            errorStatus : 0,
            error : null,
            formIsValid : false,
            currentDrug : this.props.toUpdate,

            formControls:{
                drugName:{
                    value : this.props.toUpdate.drugName,
                    valid : true,
                    touched : false,
                    validationRules:{
                        minLength : 3,
                        isRequired: true
                    }
                },
                drugDescription:{
                    value : this.props.toUpdate.drugDescription,
                    valid : true,
                    touched : false,
                    validationRules:{
                        minLength : 3,
                        isRequired: true
                    }
                },
                drugSideEffects:{
                    value : this.props.toUpdate.drugSideEffects,
                    valid : true,
                    touched : false,
                    validationRules:{
                        minLength : 3,
                        isRequired: true
                    }
                },
            }
        };
    }

    toggleForm(){
        this.setState({collapseForm: !this.state.collapseForm});
    }

    handleChange = event =>{
        const name = event.target.name;
        const value = event.target.value;
        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules); // needs a validator
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for(let updatedFormElement in updatedControls){
            formIsValid = updatedControls[updatedFormElement].valid && formIsValid;
        }

        this.setState({
            formControls : updatedControls,
            formIsValid : formIsValid
        });

    };

    handleSubmit(){
        let newDrug = new Drug();

        newDrug.drugId = this.state.currentDrug.drugId;
        newDrug.drugName = this.state.formControls.drugName.value;
        newDrug.drugDescription = this.state.formControls.drugDescription.value;
        newDrug.drugSideEffects = this.state.formControls.drugSideEffects.value;

        DrugAPI.updateDrug(newDrug, (result, status, error) =>{
            console.log(result);
                if(status === 201 || status === 200 || status == 202){
                    //ok
                    this.closeModal();
                    this.parentUpdate();

                }else{
                    this.setState(({
                        errorStatus : status,
                        error : error
                    }));
                }
        } );

    }
    render(){

        return (
            <div>

                <FormGroup id='drugName'>
                    <Label for='drugNameField'> Drug Name: </Label>
                    <Input name='drugName' id='drugNameField' placeholder = {this.state.formControls.drugName.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.drugName.value}
                           touched={this.state.formControls.drugName.touched ? 1 : 0}
                           valid = {this.state.formControls.drugName.valid}
                           required
                    />
                    {
                        this.state.formControls.drugName.touched &&
                         !this.state.formControls.drugName.valid &&
                         <div className="error-message row"> * drugName must have at least 3 characters </div>
                    }
                </FormGroup>

                <FormGroup id='drugDescription'>
                    <Label for='drugDescriptionField'> DrugDescription: </Label>
                    <Input name='drugDescription' id='drugDescriptionField' placeholder = {this.state.formControls.drugDescription.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.drugDescription.value}
                           touched={this.state.formControls.drugDescription.touched ? 1 : 0}
                           valid = {this.state.formControls.drugDescription.valid}
                           required
                    />
                    {
                        this.state.formControls.drugDescription.touched &&
                         !this.state.formControls.drugDescription.valid &&
                         <div className="error-message row"> * drugDescription must have at least 3 characters </div>
                    }
                </FormGroup>

                <FormGroup id='drugSideEffects'>
                    <Label for='drugSideEffectsField'> DrugSideEffects: </Label>
                    <Input name='drugSideEffects' id='drugSideEffectsField' placeholder = {this.state.formControls.drugSideEffects.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.drugSideEffects.value}
                           touched={this.state.formControls.drugSideEffects.touched ? 1 : 0}
                           valid = {this.state.formControls.drugSideEffects.valid}
                           required
                    />
                    {
                        this.state.formControls.drugSideEffects.touched &&
                         !this.state.formControls.drugSideEffects.valid &&
                         <div className="error-message row"> * drugSideEffects must have at least 3 characters </div>
                    }
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Update </Button>
                    </Col>
                </Row>
            </div>
        );

    }
}

export default withRouter(UpdateDrugForm);