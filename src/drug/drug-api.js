import {HOST} from '../API/hosts';
import RestApiClient from "../API/rest-client";

const endpoint = {
    updateDrugURL: '/doctor/drug/update',
    addNewDrug: '/doctor/addNewDrug'
};

function updateDrug(updatedDrug, callback){
    let request = new Request(HOST.backend_api + endpoint.updateDrugURL , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedDrug)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function addDrug(drug, callback){
    let request = new Request(HOST.backend_api + endpoint.addNewDrug , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(drug)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export{
    updateDrug,
    addDrug
};
