import React from 'react';
import Button from 'react-bootstrap/Button';
import {Col, FormGroup, Row} from 'reactstrap';
import {Form, Input, Label} from 'reactstrap';
import DrugControllerApi  from '../build/openapi/src/api/DrugControllerApi';
import PrescribedMedication from '../build/openapi/src/model/PrescribedMedication';
import validate from '../User/register-validator';
import Select from 'react-select';
import {withRouter} from 'react-router-dom';


const drugControllerApi = new DrugControllerApi();

class PrescriptionPlan extends React.Component{


    constructor(props){

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        //this.reloadHandler = this.props.reloadHandler; // nu cred ca nevoie de asa ceva aici
        
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.bindDrugs = this.bindDrugs.bind(this);
        this.closeModal = props.closeModal.bind(this);

        this.state = {
            errorStatus : 0,
            error : null,
            formIsValid : false,
            drugsLoaded: false,
            drugs:[],

            formControls:{
                intakeMoment:{
                    value : '',
                    placeholder: 'Intake moment',
                    valid : false,
                    touched : false,
                    validationRules:{
                        minLength : 3,
                        isRequired: true
                    }
                },
                dosage:{
                    value : '',
                    placeholder: 'Enter active substance value (mg)',
                    valid : false,
                    touched : false,
                    validationRules:{
                        isNumber : true,
                        isRequired: true
                    }
                },
                drug:{
                    value : null,
                    placeholder: 'Select drug',
                    valid : false,
                    touched : false,
                    validationRules:{
                        isRequired: true
                    }
                },
            }
        };
    }

    bindDrugs(){
        var dropdownData = [];
        drugControllerApi.getAllDrugs((error, data, response) =>{
            data.forEach(drug => {
                dropdownData.push({
                    value : drug,
                    label : drug.drugName
                });
            });
            this.setState({drugs : dropdownData, drugsLoaded : true});
        });
    }

    componentDidMount(){
        this.bindDrugs();
    }

    toggleForm(){
        this.setState({collapseForm: !this.state.collapseForm});
    }

    handleChange = event =>{
        const name = event.target.name;
        const value = event.target.value;
        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules); // needs a validator
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for(let updatedFormElement in updatedControls){
            formIsValid = updatedControls[updatedFormElement].valid && formIsValid;
        }

        this.setState({
            formControls : updatedControls,
            formIsValid : formIsValid
        });

    };

    handleSelectChange = (option, action) =>{          // <Select> nu returneaza un event object ci (optinuea alea, [ceva are seamana cu event])
        const name = action.name;
        const value = option.value;                    // optiunea aleasa (value, label)
        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        console.log(value);

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = true // needs a validator
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for(let updatedFormElement in updatedControls){
            formIsValid = updatedControls[updatedFormElement].valid && formIsValid;
        }

        this.setState({
            formControls : updatedControls,
            formIsValid : formIsValid
        });

    };

    handleSubmit(){
       let prescribedDrug = new PrescribedMedication();

       prescribedDrug.prescribedMedicationId = null; //pt auto increment
       prescribedDrug.dosage = this.state.formControls.dosage.value;
       prescribedDrug.intakeMoment = this.state.formControls.intakeMoment.value;
       prescribedDrug.drug = this.state.formControls.drug.value;

       this.props.handlePrescription(prescribedDrug);
       this.closeModal();

    }


    render(){

        return (
            <div>

                

                <FormGroup id='intakeMoment'>
                    <Label for='intakeMomentField'> intakeMoment: </Label>
                    <Input name='intakeMoment' id='intakeMomentField' placeholder = {this.state.formControls.intakeMoment.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.intakeMoment.value}
                           touched={this.state.formControls.intakeMoment.touched ? 1 : 0}
                           valid = {this.state.formControls.intakeMoment.valid}
                           required
                    />
                    {
                        this.state.formControls.intakeMoment.touched &&
                         !this.state.formControls.intakeMoment.valid &&
                         <div className="error-message row"> *intakeMoment must have at least 3 characters </div>
                    }
                </FormGroup>

                <FormGroup id='dosage'>
                    <Label for='dosageField'> dosage: </Label>
                    <Input name='dosage' id='dosageField' placeholder = {this.state.formControls.dosage.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.dosage.value}
                           touched={this.state.formControls.dosage.touched ? 1 : 0}
                           valid = {this.state.formControls.dosage.valid}
                           required
                    />
                    {
                        this.state.formControls.dosage.touched &&
                         !this.state.formControls.dosage.valid &&
                         <div className="error-message row"> *dosage must be a number</div>
                    }
                </FormGroup>

                <FormGroup id='drug'>
                    <Label for='drugField'> Select drug: </Label>
                    {this.state.drugsLoaded && <Select name='drug' options = {this.state.drugs} id='drugField' placeholder = {this.state.formControls.drug.placeholder}
                         onChange = {this.handleSelectChange}
                         defaultValue = {this.state.formControls.drug.value}
                         touched={this.state.formControls.drug.touched ? 1 : 0}
                         valid = {this.state.formControls.drug.valid}
                         required>
                            
                    </Select>}
                    {
                        this.state.formControls.drug.touched &&
                         !this.state.formControls.drug.valid &&
                         <div className="error-message row"> * You must select a drug </div>
                    }
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Add </Button>
                    </Col>
                </Row>
            </div>
        );

    }
}

export default withRouter(PrescriptionPlan);