import React from 'react';
import {Input, Label} from 'reactstrap';
import * as MedicationPlanAPI from './medicationPlan-api';
import validate from '../User/register-validator';
import PatientTable from '../doctor/doctor-patient/PatientTable';
import {withRouter} from 'react-router-dom';
import PrescriptionPlan from './PrescriptionForm';
import {
    Button,
    Card,
    CardHeader,
    Col,
    FormGroup,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import MedicationPlan from '../build/openapi/src/model/MedicationPlan';
import PrescribedMedication from '../build/openapi/src/model/PrescribedMedication';

  const prescriptionsColumns = [
    {
        Header: 'Name',
        accessor: 'drug.drugName',
    },
    {
        Header: 'Active subtance(mg)',
        accessor: 'dosage',
    },    
    {
        Header: 'Delete',
        accessor: 'deleteButton',
    },


];

class NewMedicationPlanForm extends React.Component{


    constructor(props){

        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeModal = props.closeModal.bind(this);
        this.formatDate = this.formatDate.bind(this);
        this.togglePrescription = this.togglePrescription.bind(this);
        this.addMedicine = this.addMedicine.bind(this);
        this.deleteMedicine = this.deleteMedicine.bind(this);

        this.state = {
            errorStatus : 0,
            error : null,
            formIsValid : false,
            patientId: this.props.patientId,
            prescribedMedication : [],
            togglePrescriptionForm: false,

            formControls:{
                startDate:{
                    value : '',
                    placeholder : 'dd/MM/yyyy',
                    valid : false,
                    touched : false,
                    validationRules:{
                        dateValidator: true,
                        isRequired: true
                    }
                },
                endDate:{
                    value : '',
                    placeholder : 'dd/MM/yyyy',
                    valid : false,
                    touched : false,
                    validationRules:{
                        dateValidator: true,
                        isRequired: true
                    }
                },
            }
        };
    }

    //https://stackoverflow.com/questions/23593052/format-javascript-date-as-yyyy-mm-dd
    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [day, month, year].join('/');
    }

    toggleForm(){
        this.setState({collapseForm: !this.state.collapseForm});
    }

    togglePrescription(){
        this.setState({togglePrescriptionForm : ! this.state.togglePrescriptionForm});
    }

    handleChange = event =>{
        const name = event.target.name;
        const value = event.target.value;
        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules); // needs a validator
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for(let updatedFormElement in updatedControls){
            formIsValid = updatedControls[updatedFormElement].valid && formIsValid;
        }

        this.setState({
            formControls : updatedControls,
            formIsValid : formIsValid
        });

    };

    handleSelectChange = (option, action) =>{          // <Select> nu returneaza un event object ci (optinuea alea, [ceva are seamana cu event])
        const name = action.name;
        const value = option.value;                    // optiunea aleasa (value, label)
        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        console.log(value);

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules); // needs a validator
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for(let updatedFormElement in updatedControls){
            formIsValid = updatedControls[updatedFormElement].valid && formIsValid;
        }

        this.setState({
            formControls : updatedControls,
            formIsValid : formIsValid
        });

    };

    handleSubmit(){
        var newMedPlan = new MedicationPlan();

        newMedPlan.medicationPlanId = null;
        newMedPlan.startDate = this.state.formControls.startDate.value;
        newMedPlan.endDate = this.state.formControls.endDate.value;

        var noDeleteButtonVersions = [];

        this.state.prescribedMedication.forEach(medicine => {
            var prescribedMedicineCopy = new PrescribedMedication();
            prescribedMedicineCopy.prescribedMedicationId = medicine.prescribedMedicationId;
            prescribedMedicineCopy.intakeMoment = medicine.intakeMoment;
            prescribedMedicineCopy.dosage = medicine.dosage;
            prescribedMedicineCopy.drug = medicine.drug;

            noDeleteButtonVersions.push(prescribedMedicineCopy);
        });
        
        newMedPlan.prescribedMedications = noDeleteButtonVersions;

        console.log(newMedPlan);

        MedicationPlanAPI.addMedicationPlan(this.state.patientId, newMedPlan, (result, status, error) =>{
            if(status === 200 || status === 202){
                this.closeModal();
                console.log('Success! New medication plan was added');
            }else{

            }
        });

    }

    deleteMedicine(medicine){
        var allMedication = this.state.prescribedMedication;
        if(allMedication.indexOf(medicine) !== -1){
            allMedication.splice(allMedication.indexOf(medicine), 1); // removes element form array
        }
        this.setState({prescribedMedication : allMedication});
    }

    addMedicine(medicine){
        var allMedication = this.state.prescribedMedication;
        allMedication.push(medicine);
        this.setState({prescribedMedication : allMedication});
    }

    render(){

        var currentMedicine = this.state.prescribedMedication.slice(0);

        currentMedicine.forEach(medicine => {
            medicine.deleteButton = <Button onClick = {()=>{this.deleteMedicine(medicine)}}>Delete</Button>;
        });

        return (
            <div>


                <FormGroup id='startDate'>
                    <Label for='startDateField'> Start Date: </Label>
                    <Input name='startDate' id='startDateField' placeholder = {this.state.formControls.startDate.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.startDate.value}
                           touched={this.state.formControls.startDate.touched ? 1 : 0}
                           valid = {this.state.formControls.startDate.valid}
                           required
                    />
                    {
                        this.state.formControls.startDate.touched &&
                         !this.state.formControls.startDate.valid &&
                         <div className="error-message row"> * First name must have at least 3 characters </div>
                    }
                </FormGroup>

                <FormGroup id='endDate'>
                    <Label for='endDateField'> End Date: </Label>
                    <Input name='endDate' id='endDateField' placeholder = {this.state.formControls.endDate.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.endDate.value}
                           touched={this.state.formControls.endDate.touched ? 1 : 0}
                           valid = {this.state.formControls.endDate.valid}
                           required
                    />
                    {
                        this.state.formControls.endDate.touched &&
                         !this.state.formControls.endDate.valid &&
                         <div className="error-message row"> * First name must have at least 3 characters </div>
                    }
                </FormGroup>

                <CardHeader>
                    <strong> Prescribed Medicine </strong>
                </CardHeader>
                <Card>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                           <PatientTable tableData = {currentMedicine} desiredColumns = {prescriptionsColumns}></PatientTable>
                        </Col>
                    </Row>
                </Card>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"}  onClick={this.togglePrescription}>  Prescribe Medicine </Button>
                    </Col>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Add  </Button>
                    </Col>
                </Row>

                <Modal isOpen={this.state.togglePrescriptionForm} toggle={this.togglePrescription}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.togglePrescription}> Add medication plan: </ModalHeader>
                    <ModalBody>
                        <h1>Prescription modal</h1>
                        <PrescriptionPlan handlePrescription={this.addMedicine} closeModal={this.togglePrescription}></PrescriptionPlan>
                    </ModalBody>
                </Modal>
            </div>
        );

    }
}

export default withRouter(NewMedicationPlanForm);