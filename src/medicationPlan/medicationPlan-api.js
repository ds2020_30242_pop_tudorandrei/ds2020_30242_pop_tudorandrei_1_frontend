import {HOST} from '../API/hosts';
import RestApiClient from "../API/rest-client";

const endpoint = {
    addMedicationPlan : '/doctor/patient/createMedicationPlan/'
};

function addMedicationPlan(patientId, medicationPlan, callback){
    let request = new Request(HOST.backend_api + endpoint.addMedicationPlan +patientId, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicationPlan)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export{
    addMedicationPlan
};
