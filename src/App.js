import React from 'react';
import logo from './logo.svg';
import './App.css';
import UserControllerApi from './build/openapi/src/api/UserControllerApi';
import DoctorControllerApi from './build/openapi/src/api/DoctorControllerApi';
import { useOperation } from 'react-openapi-client';
import IsUser from './build/openapi/src/model/IsUser';
import RegisterForm from './User/RegisterForm';
import UserLogInForm from './User/UserLogInForm';
import Home from './home/Home'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';


function App() {
 
  return (
    <div>
      <Router>
        <Home/>
      </Router>
    </div>
  );
  
}

export default App;
