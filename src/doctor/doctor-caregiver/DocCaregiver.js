import React from 'react';
import PatientTable from '../doctor-patient/PatientTable';
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import DoctorControllerApi from '../../build/openapi/src/api/DoctorControllerApi';
import CaregiverControllerApi from '../../build/openapi/src/api/CaregiverControllerApi';
import UpdateForm from '../../User/userUpdateForm';
import AssignPatientsCG from './AssignPatientsCG';

const unassignedColumns = [
    {
        Header: 'FirstName',
        accessor: 'user.userFirstName',
    },
    {
        Header: 'LastName',
        accessor: 'user.userLastName',
    },
    {
        Header: 'Username',
        accessor: 'user.userName',
    },
    {
        Header: 'Hire',
        accessor: 'assignButton',
    },
    {
        Header: 'Update',
        accessor: 'updateButton',
    },
    {
        Header: 'Delete',
        accessor: 'deleteButton',
    }

];

const assignedColumns = [
    {
        Header: 'FirstName',
        accessor: 'user.userFirstName',
    },
    {
        Header: 'LastName',
        accessor: 'user.userLastName',
    },
    {
        Header: 'Username',
        accessor: 'user.userName',
    },
    {
        Header: 'Dismiss',
        accessor: 'dismissButton',
    },
    {
        Header: 'AssignPatients',
        accessor: 'assignPatientsButton',
    },
    {
        Header: 'Update',
        accessor: 'updateButton',
    }

];

const caregiverControllerApi = new CaregiverControllerApi();
const doctorControllerApi = new DoctorControllerApi();

class DocCaregiver extends React.Component {

    constructor(props) {
        super(props);
        this.toggleAddPatients = this.toggleAddPatients.bind(this);
        this.state ={
            docLoaded : false,
            unassignedCaregiversLoaded : false,
            assignedCaregiversLoaded : false,
            unassignedCaregivers : [],
            assignedCaregivers : [],
            doctor : JSON.parse(window.localStorage.getItem('user')),
            doctorId : JSON.parse(window.localStorage.getItem('userId')),
            updateSelected : false,
            toUpdate : null,
            addPatientsSelected : false,
            assgniedCaregiverId : null, 

        }
        this.toggleUpdateForm = this.toggleUpdateForm.bind(this);
        //this.toggleMedPlanForm = this.toggleMedPlanForm.bind(this);
    }

    bindDoctor(){
        if(this.state.doctor !== undefined){
            this.setState({docLoaded : true});
        }
    }

    bindUnassignedCaregivers(){
        caregiverControllerApi.getCaregivers((error, data, response) =>{
            if(data != null){
                var freeCaregivers = [];
                data.forEach(caregiver => {
                    if(caregiver.doctor == null){
                        freeCaregivers.push(caregiver);
                    }
                });
                this.setState({unassignedCaregivers : freeCaregivers, unassignedCaregiversLoaded: true});
            }
        });
    }

    componentDidMount(){
        this.bindDoctor();
        this.bindUnassignedCaregivers();
    }

    refreshData(){
        var doctorId = window.localStorage.getItem('userId');
        doctorControllerApi.getDoctorById(doctorId,(error, data, response)=>{
            this.setState({doctor : data});
        });
        this.bindUnassignedCaregivers();
    }

    hireCaregiver(caregiverId, doctorId){
        doctorControllerApi.hireCaregiver(caregiverId, doctorId,(error, data, response)=>{
            this.refreshData();
        });
    }

    dismissCaregiver(caregiverId, doctorId){
        doctorControllerApi.dismissCaregiver(caregiverId, doctorId,(error, data, response)=>{
            this.refreshData();
        });
    }

    deleteCaregiver(caregiverId){
        doctorControllerApi.deleteCaregiver(caregiverId, (error, data, response)=>{
            this.refreshData();
        });
    }

    toggleUpdateForm(user){
        this.setState({updateSelected : ! this.state.updateSelected, toUpdate : user});
    }

    toggleAddPatients(caregiverId){
        this.setState({addPatientsSelected : !this.state.addPatientsSelected, assgniedCaregiverId: caregiverId});
    }

    render() {

       if(this.state.docLoaded ){
       
            var hiredCaregivers = this.state.doctor.caregivers.slice(0);
            hiredCaregivers.forEach(hiredCaregiver => {
                hiredCaregiver.dismissButton = <Button onClick={()=>{this.dismissCaregiver(hiredCaregiver.caregiverId, this.state.doctor.doctorId)}}>Dismiss</Button>;
                hiredCaregiver.assignPatientsButton = <Button onClick = {()=>{this.toggleAddPatients(hiredCaregiver.caregiverId);}}>AssignPatientsButton</Button>;
                hiredCaregiver.updateButton = <Button onClick={()=>{this.toggleUpdateForm(hiredCaregiver.user);}}>Update</Button>;
            });
            console.log(hiredCaregivers);

            var notHiredCaregivers = this.state.unassignedCaregivers.slice(0);
            notHiredCaregivers.forEach(notHiredCaregiver => {
                notHiredCaregiver.assignButton = <Button onClick={()=>{this.hireCaregiver(notHiredCaregiver.caregiverId, this.state.doctor.doctorId)}}>Assign</Button>;
                notHiredCaregiver.updateButton = <Button onClick={()=>{this.toggleUpdateForm(notHiredCaregiver.user);}}>Update</Button>;
                notHiredCaregiver.deleteButton = <Button onClick = {()=>{this.deleteCaregiver(notHiredCaregiver.caregiverId)}}>Delete</Button>;
            });
            notHiredCaregivers = notHiredCaregivers.filter((notHiredCaregiver)=>{return notHiredCaregiver.caregiverId !== undefined});
       }
        return (

            <div>
                <CardHeader>
                    <strong> Hired caregivers </strong>
                </CardHeader>
                <Card>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                           {this.state.docLoaded && <PatientTable tableData = {hiredCaregivers} desiredColumns = {assignedColumns}></PatientTable>} 
                        </Col>
                    </Row>
                </Card>
                <CardHeader>
                    <strong> Unassigned Caregivers </strong>
                </CardHeader>
                <Card>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                           {this.state.unassignedCaregiversLoaded && <PatientTable tableData = {notHiredCaregivers} desiredColumns = {unassignedColumns}></PatientTable>} 
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.updateSelected} toggle={this.toggleUpdateForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateForm}> UpdateCaregiver: </ModalHeader>
                    <ModalBody>
                        <UpdateForm user = {this.state.toUpdate} closeModal ={this.toggleUpdateForm}></UpdateForm>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.addPatientsSelected} toggle={this.toggleAddPatients}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddPatients}> UpdateCaregiver: </ModalHeader>
                    <ModalBody>
                        <h1>Add patients</h1>
                        <AssignPatientsCG caregiverId = {this.state.assgniedCaregiverId} doctorId = {this.state.doctor.doctorId}></AssignPatientsCG>
                    </ModalBody>
                </Modal>

            </div>
        )
    }
}

export default DocCaregiver;