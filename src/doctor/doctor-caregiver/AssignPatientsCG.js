import React from 'react';
import PatientTable from '../doctor-patient/PatientTable';
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import DoctorControllerApi from '../../build/openapi/src/api/DoctorControllerApi';
import PatientControllerApi from '../../build/openapi/src/api/PatientControllerApi';
import CaregiverControllerApi from '../../build/openapi/src/api/CaregiverControllerApi';

const unassignedColumns = [
    {
        Header: 'FirstName',
        accessor: 'user.userFirstName',
    },
    {
        Header: 'LastName',
        accessor: 'user.userLastName',
    },
    {
        Header: 'Username',
        accessor: 'user.userName',
    },
    {
        Header: 'Assign',
        accessor: 'assignButton',
    },

];

const assignedColumns = [
    {
        Header: 'FirstName',
        accessor: 'user.userFirstName',
    },
    {
        Header: 'LastName',
        accessor: 'user.userLastName',
    },
    {
        Header: 'Username',
        accessor: 'user.userName',
    },
    {
        Header: 'Dismiss',
        accessor: 'dismissButton',
    },

];

const patientControllerApi = new PatientControllerApi();
const doctorControllerApi = new DoctorControllerApi();
const caregiverControllerApi = new CaregiverControllerApi();

class AssignPatientsCG extends React.Component {

    constructor(props) {
        super(props);
        this.dismissPatient = this.dismissPatient.bind(this);
        this.assignPatient = this.assignPatient.bind(this);
        this.state ={
            docLoaded : false,
            unassignedPatientsLoaded : false,
            assignedPatientsLoaded : false,
            unassignedPatients : [],
            assignedPatients : [],
            //doctor : JSON.parse(window.localStorage.getItem('user')),
            doctorId : props.doctorId,
            caregiverId : props.caregiverId,

        }
    }

    bindDoctor(){
        if(this.state.doctor !== undefined){
            this.setState({docLoaded : true});
        }
    }

    bindUnassignedPatients(){
        doctorControllerApi.getDoctorById(this.state.doctorId,(error, data, response)=>{
            var freePatients = [];
            console.log(data.patients);
            if(data != null){
                
                data.patients.forEach(patient => {
                    if(patient.caregiver == null){
                        freePatients.push(patient);
                    }
                });
                
            }
            this.setState({unassignedPatients : freePatients, unassignedPatientsLoaded: true});
        });
    }

    bindAssignedPatients(){
        caregiverControllerApi.findAssignedPatientsById(this.state.caregiverId, (error, data, response) =>{
            this.setState({assignedPatients : data, assignedPatientsLoaded: true});
        });
    }

    componentDidMount(){
        this.bindDoctor();
        this.bindUnassignedPatients();
        this.bindAssignedPatients();
    }

    refreshData(){
        // var doctorId = window.localStorage.getItem('userId');
        
        this.bindUnassignedPatients();
        this.bindAssignedPatients();
    }

    assignPatient(patientId, caregiverId){
        doctorControllerApi.assignPatient(patientId, caregiverId,(error, data, response)=>{
            this.refreshData();
        });
    }

    dismissPatient(patientId, caregiverId){
        doctorControllerApi.dismissCaregiverPatient(patientId, caregiverId,(error, data, response)=>{
            this.refreshData();
        });
    }

    
    render() {
        var treatedPatients = [];
        var untreatedPatients = [];

       if(this.state.assignedPatientsLoaded && this.state.unassignedPatientsLoaded){
       
            treatedPatients = this.state.assignedPatients.slice(0);
            treatedPatients.forEach(treatedPatient => {
                treatedPatient.dismissButton = <Button onClick={()=>{this.dismissPatient(treatedPatient.patientId, this.state.caregiverId)}}>Dismiss</Button>;
            });
            //treatedPatients = treatedPatients.filter((treatedPatients)=>{return treatedPatients.patientId !== undefined});

            untreatedPatients = this.state.unassignedPatients.slice(0);
            untreatedPatients.forEach(untreatedPatient => {
                untreatedPatient.assignButton = <Button onClick={()=>{this.assignPatient(untreatedPatient.patientId, this.state.caregiverId)}}>Assign</Button>;
            });
            untreatedPatients = untreatedPatients.filter((untreatedPatient)=>{return untreatedPatient.patientId !== undefined});
       }
        return (

            <div>
                <CardHeader>
                    <strong> Treated Patients </strong>
                </CardHeader>
                <Card>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                           {this.state.assignedPatientsLoaded && <PatientTable tableData = {treatedPatients} desiredColumns = {assignedColumns}></PatientTable>} 
                        </Col>
                    </Row>
                </Card>
                <CardHeader>
                    <strong> Unassigned Patients </strong>
                </CardHeader>
                <Card>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                           {this.state.unassignedPatientsLoaded && <PatientTable tableData = {untreatedPatients} desiredColumns = {unassignedColumns}></PatientTable>} 
                        </Col>
                    </Row>
                </Card>
            </div>
        )
    }
}

export default AssignPatientsCG;