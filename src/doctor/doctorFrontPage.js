import React from 'react'
import {withRouter, Route, Switch} from 'react-router-dom';
import DoctorNavBar from './doctorNavBar';
import DoctorControllerApi from '../build/openapi/src/api/DoctorControllerApi'
import DocPatient from './doctor-patient/DocPatient'
import DocCaregiver from './doctor-caregiver/DocCaregiver'
import DocDrug from './doctor-drug/DocDrug';

var doctorApi = new DoctorControllerApi();
class DoctorFrontPage extends React.Component {

    
    constructor(props){
        
        super(props);
        this.doctorId = props.componentId;

        this.state = {
            loggedIn : false,
            doctor : JSON.parse(window.localStorage.getItem('user'))
        };
    }

    
    bindDoctor(){

        if(this.state.doctor === undefined || this.state.doctor === null)
        {
            doctorApi.getDoctorById(this.doctorId, (err, data, resp)=>{
                this.setState({doctor : data});
                window.localStorage.setItem('user', JSON.stringify(this.state.doctor));
                window.localStorage.setItem('userId', JSON.stringify(this.state.doctor.doctorId));
            });
        }
    }

    componentDidMount(){
       this.bindDoctor();
    }

    render() {
        console.log(this.state.doctor);
        return (
        <div>
            <DoctorNavBar handleLogOut= {this.props.handleLogOut}/>
            
            {this.state.doctor != null && <Switch>
                <Route
                    path='/doctor/patient'
                    exact
                    component = {DocPatient}
                />

                <Route
                    path='/doctor/caregiver'
                    exact
                    component = {DocCaregiver}
                />

                <Route
                    path='/doctor/drug'
                    exact
                    component = {DocDrug}
                />

                <Route
                    path='/doctor/info'
                    exact
                    component = {() =>{<br></br>} }
                />
             </Switch>}
        </div>

        )
    };
}

export default withRouter(DoctorFrontPage);
