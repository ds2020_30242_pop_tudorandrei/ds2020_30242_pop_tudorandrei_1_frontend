import React from 'react';
import PatientTable from './PatientTable';
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import DoctorControllerApi from '../../build/openapi/src/api/DoctorControllerApi';
import PatientControllerApi from '../../build/openapi/src/api/PatientControllerApi';
import UpdateForm from '../../User/userUpdateForm';
import NewMedicationPlanForm from '../../medicationPlan/newMedicationPlan';

const unassignedColumns = [
    {
        Header: 'FirstName',
        accessor: 'user.userFirstName',
    },
    {
        Header: 'LastName',
        accessor: 'user.userLastName',
    },
    {
        Header: 'Username',
        accessor: 'user.userName',
    },
    {
        Header: 'Assign',
        accessor: 'assignButton',
    },
    {
        Header: 'Update',
        accessor: 'updateButton',
    },
    {
        Header: 'Delete',
        accessor: 'deleteButton',
    }

];

const assignedColumns = [
    {
        Header: 'FirstName',
        accessor: 'user.userFirstName',
    },
    {
        Header: 'LastName',
        accessor: 'user.userLastName',
    },
    {
        Header: 'Username',
        accessor: 'user.userName',
    },
    {
        Header: 'Dismiss',
        accessor: 'dismissButton',
    },
    {
        Header: 'MedicationPlan',
        accessor: 'addMedicationPlanButton',
    },
    {
        Header: 'Update',
        accessor: 'updateButton',
    }

];

const patientControllerApi = new PatientControllerApi();
const doctorControllerApi = new DoctorControllerApi();

class DocPatient extends React.Component {

    constructor(props) {
        super(props);
        this.state ={
            docLoaded : false,
            unassignedPatientsLoaded : false,
            assignedPatientsLoaded : false,
            unassignedPatients : [],
            assignedPatients : [],
            doctor : JSON.parse(window.localStorage.getItem('user')),
            doctorId : JSON.parse(window.localStorage.getItem('userId')),
            updateSelected : false,
            medPlanSelected : false,
            toUpdate : null,
            medicatedPatientId : null,

        }
        this.toggleUpdateForm = this.toggleUpdateForm.bind(this);
        this.toggleMedPlanForm = this.toggleMedPlanForm.bind(this);
    }

    bindDoctor(){
        if(this.state.doctor !== undefined){
            this.setState({docLoaded : true});
        }
    }

    bindUnassignedPatients(){
        patientControllerApi.getPatients((error, data, response) =>{
            if(data != null){
                var freePatients = [];
                data.forEach(patient => {
                    if(patient.doctor == null){
                        freePatients.push(patient);
                    }
                });
                this.setState({unassignedPatients : freePatients, unassignedPatientsLoaded: true});
            }
        });
    }

    componentDidMount(){
        this.bindDoctor();
        this.bindUnassignedPatients();
    }

    refreshData(){
        var doctorId = window.localStorage.getItem('userId');
        doctorControllerApi.getDoctorById(doctorId,(error, data, response)=>{
            this.setState({doctor : data});
        });
        this.bindUnassignedPatients();
    }

    assignPatient(patientId, doctorId){
        doctorControllerApi.assignDoctor(patientId, doctorId,(error, data, response)=>{
            this.refreshData();
        });
    }

    dismissPatient(patientId, doctorId){
        doctorControllerApi.dismissPatient(patientId, doctorId,(error, data, response)=>{
            this.refreshData();
        });
    }

    deletePatient(patientId){
        doctorControllerApi.deletePatient(patientId, (error, data, response)=>{
            this.refreshData();
        });
    }

    toggleUpdateForm(user){
        this.setState({updateSelected : ! this.state.updateSelected, toUpdate : user});
    }

    toggleMedPlanForm(patientId){
        this.setState({medPlanSelected : ! this.state.medPlanSelected, medicatedPatientId : patientId});
    }
    render() {

       if(this.state.docLoaded ){
       
            var treatedPatients = this.state.doctor.patients.slice(0);
            treatedPatients.forEach(treatedPatient => {
                treatedPatient.dismissButton = <Button onClick={()=>{this.dismissPatient(treatedPatient.patientId, this.state.doctor.doctorId)}}>Dismiss</Button>;
                treatedPatient.addMedicationPlanButton = <Button onClick = {()=>{this.toggleMedPlanForm(treatedPatient.patientId);}}>Add Medication Plan</Button>;
                treatedPatient.updateButton = <Button onClick={()=>{this.toggleUpdateForm(treatedPatient.user);}}>Update</Button>;
            });
            console.log(treatedPatients);

            var untreatedPatients = this.state.unassignedPatients.slice(0);
            untreatedPatients.forEach(untreatedPatient => {
                untreatedPatient.assignButton = <Button onClick={()=>{this.assignPatient(untreatedPatient.patientId, this.state.doctor.doctorId)}}>Assign</Button>;
                untreatedPatient.updateButton = <Button onClick={()=>{this.toggleUpdateForm(untreatedPatient.user);}}>Update</Button>;
                untreatedPatient.deleteButton = <Button onClick = {()=>{this.deletePatient(untreatedPatient.patientId)}}>Delete</Button>;
            });
            untreatedPatients = untreatedPatients.filter((untreatedPatient)=>{return untreatedPatient.patientId !== undefined});
       }
        return (

            <div>
                <CardHeader>
                    <strong> Treated Patients </strong>
                </CardHeader>
                <Card>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                           {this.state.docLoaded && <PatientTable tableData = {treatedPatients} desiredColumns = {assignedColumns}></PatientTable>} 
                        </Col>
                    </Row>
                </Card>
                <CardHeader>
                    <strong> Unassigned Patients </strong>
                </CardHeader>
                <Card>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                           {this.state.unassignedPatientsLoaded && <PatientTable tableData = {untreatedPatients} desiredColumns = {unassignedColumns}></PatientTable>} 
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.updateSelected} toggle={this.toggleUpdateForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateForm}> UpdatePatient: </ModalHeader>
                    <ModalBody>
                        <UpdateForm user = {this.state.toUpdate} closeModal ={this.toggleUpdateForm}></UpdateForm>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.medPlanSelected} toggle={this.toggleMedPlanForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleMedPlanForm}> Add medication plan: </ModalHeader>
                    <ModalBody>
                        <h1>MedicationPlan</h1>
                        <NewMedicationPlanForm patientId ={this.state.medicatedPatientId} closeModal ={this.toggleMedPlanForm}></NewMedicationPlanForm>
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}

export default DocPatient;