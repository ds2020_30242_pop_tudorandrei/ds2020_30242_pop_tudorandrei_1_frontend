import React from "react";
import {useTable} from 'react-table';
import {Table} from "react-bootstrap";


// https://www.youtube.com/watch?v=hson9BXU9F8 God, bless this person!
function PatientTable(props){
    
   // console.log(props.desiredColumns)
    //console.log(props.tableData)
    const tableInstance = useTable({
        columns : props.desiredColumns,
        data: props.tableData
    });

    const{
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow
    } = tableInstance;

     return (
             
        <div>
            <Table {...getTableProps()} striped bordered hover>
                <thead>
                    {
                        headerGroups.map((headerGroup)=>(
                            <tr {...headerGroup.getHeaderGroupProps()}>
                                {
                                    headerGroup.headers.map((column) =>(
                                        <th {...column.getHeaderProps()}>{column.render('Header')}</th>
                                    ))
                                }
                                
                            </tr>
                        ))
                    }
                    
                </thead>
                <tbody {...getTableBodyProps()}>
                    {
                        rows.map(row =>{
                            prepareRow(row)
                            return(
                                <tr {...row.getRowProps()}>
                                    {
                                        row.cells.map(cell =>{
                                        return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                        })
                                    }
                                    
                                </tr>
                            )
                        })
                    }
                    
                </tbody>
            </Table>
        </div>
    );
}

export default PatientTable;