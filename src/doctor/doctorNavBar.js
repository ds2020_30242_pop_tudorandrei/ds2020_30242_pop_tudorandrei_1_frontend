import React from 'react'
import logo from '../commons/images/icon.png';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown,
    Button
} from 'reactstrap';
import {withRouter,Link } from 'react-router-dom';

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

class DoctorNavBar extends React.Component{
    constructor(props){

        super(props);
        this.handleLogOut = props.handleLogOut.bind(this);
        // this.goIdle = props.goIdle.bind(this);
        // this.pickFunction = props.pickFunction.bind(this);
    }


    render(){
        return (
            <div>
                <Navbar color="dark" light expand="md">
                    <NavbarBrand href="/doctor">
                        <img src={logo} width={"50"}
                             height={"35"} />
                    </NavbarBrand>
                    <Nav className="mr-auto" navbar>
        
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle style={textStyle} nav caret>
                               Menu
                            </DropdownToggle>
                            <DropdownMenu right >
        
                                <DropdownItem>
                                    <NavLink href='/doctor/patient'>Patients</NavLink>
                                </DropdownItem>
                                <DropdownItem>
                                    <NavLink href="/doctor/caregiver">Caregivers</NavLink>
                                </DropdownItem>
                                <DropdownItem>
                                    <NavLink href="/doctor/drug">Drugs</NavLink>
                                </DropdownItem>
                                <DropdownItem>
                                    <NavLink href="/doctor/info">UserInfo</NavLink>
                                </DropdownItem>
        
                            </DropdownMenu>
                        </UncontrolledDropdown>
        
                        <div>

                            <Button color="primary" className = "col col-lg-2" onClick = {() =>{
                                        this.props.history.push('/'); //change path
                                        this.handleLogOut(); // logout
                                    }}>LogOut</Button>
            
                        </div>
                    </Nav>
                </Navbar>
            </div>
        );
    }
}
export default withRouter(DoctorNavBar);