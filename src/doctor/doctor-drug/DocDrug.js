import React from 'react';
import PatientTable from '../doctor-patient/PatientTable';
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import DrugControllerApi from '../../build/openapi/src/api/DrugControllerApi';
import DoctorControllerApi from '../../build/openapi/src/api/DoctorControllerApi';
import Drug from '../../build/openapi/src/model/Drug';
import AddDrugForm from '../../drug/AddDrugForm';
import UpdateDrugForm from '../../drug/UpdateDrugForm';

const DrugTableColumns = [
    {
        Header: 'DrugName',
        accessor: 'drugName',
    },
    {
        Header: 'DrugDescription',
        accessor: 'drugDescription',
    },
    {
        Header: 'DrugSideEffects',
        accessor: 'drugSideEffects',
    },
    {
        Header: 'Delete',
        accessor: 'deleteButton',
    },
    {
        Header: 'Update',
        accessor: 'updateButton',
    }

];

const drugControllerApi = new DrugControllerApi();
const doctorControllerApi = new DoctorControllerApi();

class DocDrug extends React.Component {

   
    constructor(props) {
        super(props);

        this.toggleAddForm = this.toggleAddForm.bind(this);
        this.toggleUpdateForm = this.toggleUpdateForm.bind(this);
        this.bindDoctor = this.bindDoctor.bind(this);
        this.bindDrugs = this.bindDrugs.bind(this);
        this.refreshData = this.refreshData.bind(this);
        this.deleteDrug = this.deleteDrug.bind(this);
        this.cloneDrug = this.cloneDrug.bind(this);


        this.state ={
            docLoaded : false,
            drugsLoaded : false,
            addDrugSelected : false,
            drugs : [],
            doctor : JSON.parse(window.localStorage.getItem('user')),
            doctorId : JSON.parse(window.localStorage.getItem('userId')),
            updateSelected : false,
            toUpdate : null,

        }
        this.toggleUpdateForm = this.toggleUpdateForm.bind(this);
    }

    bindDoctor(){
        if(this.state.doctor !== undefined){
            this.setState({docLoaded : true});
        }
    }

    bindDrugs(){
        drugControllerApi.getAllDrugs((error, data, response) =>{
            if(data != null){
                this.setState({drugs : data, drugsLoaded: true});
            }
        });
    }

    componentDidMount(){
        this.bindDoctor();
        this.bindDrugs();
    }

    refreshData(){
        this.bindDrugs();
    }


    deleteDrug(drugId){
        doctorControllerApi.deleteDrug(drugId, (error, data, response)=>{
            this.refreshData();
        });
    }

    toggleUpdateForm(drug){
        this.setState({updateSelected : ! this.state.updateSelected, toUpdate : drug});
    }

    toggleAddForm(){
        this.setState({addDrugSelected : ! this.state.addDrugSelected});
    }

    cloneDrug(drug){
        var newDrug = new Drug();
        newDrug.drugId = drug.drugId;
        newDrug.drugName = drug.drugName;
        newDrug.drugDescription = drug.drugDescription;

        return newDrug;
    }
    render() {

       if(this.state.docLoaded && this.state.drugsLoaded){
       
            var allDrugs = this.state.drugs.slice(0);
            allDrugs.forEach(drug => {
                var drugCopy = this.cloneDrug(drug);
                drug.deleteButton = <Button onClick={()=>{this.deleteDrug(drug.drugId);}}>Delete</Button>;
                drug.updateButton = <Button onClick={()=>{this.toggleUpdateForm(drugCopy);}}>Update</Button>;
            });
            console.log(allDrugs);
       }
        return (

            <div>
                <CardHeader>
                    <strong> Add Drug </strong>
                    <Button onClick={this.toggleAddForm}>Add</Button>
                </CardHeader>
                <Card>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                           {this.state.drugsLoaded && <PatientTable tableData = {allDrugs} desiredColumns = {DrugTableColumns}></PatientTable>} 
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.updateSelected} toggle={this.toggleUpdateForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateForm}> UpdateDrug: </ModalHeader>
                    <ModalBody>
                        <h1>Update Drug</h1>
                        <UpdateDrugForm toUpdate = {this.state.toUpdate} closeModal ={this.toggleUpdateForm} parentUpdate = {this.bindDrugs}></UpdateDrugForm>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.addDrugSelected} toggle={this.toggleAddForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddForm}> Add new drug: </ModalHeader>
                    <ModalBody>
                        <h1>Update Drug</h1>
                        <AddDrugForm parentUpdate = {this.bindDrugs} closeModal ={this.toggleAddForm}></AddDrugForm>
                    </ModalBody>
                </Modal>



            </div>
        )
    }
}

export default DocDrug;