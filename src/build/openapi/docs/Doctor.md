# OpenApiDefinition.Doctor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**doctorId** | **Number** |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**patients** | [**[Patient]**](Patient.md) |  | [optional] 
**caregivers** | [**[Caregiver]**](Caregiver.md) |  | [optional] 


