# OpenApiDefinition.PatientControllerApi

All URIs are relative to *https://spring-demo-ds2020-pls-work.herokuapp.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getMedicationPlans**](PatientControllerApi.md#getMedicationPlans) | **GET** /patient/getMedicationPlans/{patientId} | 
[**getPatientById**](PatientControllerApi.md#getPatientById) | **GET** /patient/getById/{patientId} | 
[**getPatients**](PatientControllerApi.md#getPatients) | **GET** /patient/getAll | 



## getMedicationPlans

> [MedicationPlan] getMedicationPlans(patientId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.PatientControllerApi();
let patientId = 789; // Number | 
apiInstance.getMedicationPlans(patientId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patientId** | **Number**|  | 

### Return type

[**[MedicationPlan]**](MedicationPlan.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## getPatientById

> Patient getPatientById(patientId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.PatientControllerApi();
let patientId = 789; // Number | 
apiInstance.getPatientById(patientId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patientId** | **Number**|  | 

### Return type

[**Patient**](Patient.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## getPatients

> [Patient] getPatients()



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.PatientControllerApi();
apiInstance.getPatients((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[Patient]**](Patient.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

