# OpenApiDefinition.UserDetailsDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**firstName** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**birthDate** | **Date** |  | [optional] 
**address** | **String** |  | [optional] 
**gender** | **String** |  | [optional] 


