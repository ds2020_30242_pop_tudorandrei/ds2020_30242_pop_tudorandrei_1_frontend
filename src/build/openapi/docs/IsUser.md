# OpenApiDefinition.IsUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **String** |  | [optional] 
**userName** | **String** |  | [optional] 
**userPassword** | **String** |  | [optional] 
**userRole** | **String** |  | [optional] 



## Enum: UserRoleEnum


* `PATIENT` (value: `"PATIENT"`)

* `DOCTOR` (value: `"DOCTOR"`)

* `CAREGIVER` (value: `"CAREGIVER"`)




