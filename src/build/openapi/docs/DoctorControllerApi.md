# OpenApiDefinition.DoctorControllerApi

All URIs are relative to *https://spring-demo-ds2020-pls-work.herokuapp.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addMedicationPlan**](DoctorControllerApi.md#addMedicationPlan) | **POST** /doctor/patient/createMedicationPlan/{patientId} | 
[**assignCaregiver**](DoctorControllerApi.md#assignCaregiver) | **POST** /doctor/patient/assignCaregiver/{caregiverId} | 
[**assignDoctor**](DoctorControllerApi.md#assignDoctor) | **POST** /doctor/addPatient/{patientId}/{doctorId} | 
[**assignPatient**](DoctorControllerApi.md#assignPatient) | **POST** /doctor/caregiver/assignPatient/{patientId}/{caregiverId} | 
[**deleteCaregiver**](DoctorControllerApi.md#deleteCaregiver) | **DELETE** /doctor/caregiver/delete/{caregiverId} | 
[**deleteDrug**](DoctorControllerApi.md#deleteDrug) | **DELETE** /doctor/drug/delete/{drugId} | 
[**deletePatient**](DoctorControllerApi.md#deletePatient) | **DELETE** /doctor/patient/delete/{patientId} | 
[**dismissCaregiver**](DoctorControllerApi.md#dismissCaregiver) | **POST** /doctor/caregiver/dismiss/{caregiverId}/{doctorId} | 
[**dismissCaregiverPatient**](DoctorControllerApi.md#dismissCaregiverPatient) | **DELETE** /doctor/caregiver/dismissPatient/{patientId}/{caregiverId} | 
[**dismissPatient**](DoctorControllerApi.md#dismissPatient) | **POST** /doctor/dismissPatient/{patientId}/{doctorId} | 
[**findAllDoctors**](DoctorControllerApi.md#findAllDoctors) | **GET** /doctor/findAll | 
[**getDoctorById**](DoctorControllerApi.md#getDoctorById) | **GET** /doctor/getById/{doctorId} | 
[**getHiredCaregivers**](DoctorControllerApi.md#getHiredCaregivers) | **GET** /doctor/caregiver/getHiredCaregivers/{doctorId} | 
[**hireCaregiver**](DoctorControllerApi.md#hireCaregiver) | **POST** /doctor/caregiver/hireCaregiver/{caregiverId}/{doctorId} | 
[**insertNewDrug**](DoctorControllerApi.md#insertNewDrug) | **POST** /doctor/addNewDrug | 
[**prescribeMedication**](DoctorControllerApi.md#prescribeMedication) | **POST** /doctor/patient/prescribe/{patientId}/{medicationPlanId} | 
[**undoMedication**](DoctorControllerApi.md#undoMedication) | **POST** /doctor/patient/deletePrescription/{patientId}/{medicationPlanId} | 
[**updateCaregiver**](DoctorControllerApi.md#updateCaregiver) | **POST** /doctor/caregiver/update | 
[**updateDrug**](DoctorControllerApi.md#updateDrug) | **POST** /doctor/drug/update | 
[**updatePatient**](DoctorControllerApi.md#updatePatient) | **POST** /doctor/patient/update | 
[**viewTreatedPatients**](DoctorControllerApi.md#viewTreatedPatients) | **GET** /doctor/patient/getTreatedPatients/{doctorId} | 



## addMedicationPlan

> MedicationPlan addMedicationPlan(patientId, opts)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let patientId = 789; // Number | 
let opts = {
  'medicationPlan': new OpenApiDefinition.MedicationPlan() // MedicationPlan | 
};
apiInstance.addMedicationPlan(patientId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patientId** | **Number**|  | 
 **medicationPlan** | [**MedicationPlan**](MedicationPlan.md)|  | [optional] 

### Return type

[**MedicationPlan**](MedicationPlan.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*


## assignCaregiver

> Number assignCaregiver(caregiverId, opts)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let caregiverId = 789; // Number | 
let opts = {
  'patient': new OpenApiDefinition.Patient() // Patient | 
};
apiInstance.assignCaregiver(caregiverId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caregiverId** | **Number**|  | 
 **patient** | [**Patient**](Patient.md)|  | [optional] 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*


## assignDoctor

> Number assignDoctor(patientId, doctorId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let patientId = 789; // Number | 
let doctorId = 789; // Number | 
apiInstance.assignDoctor(patientId, doctorId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patientId** | **Number**|  | 
 **doctorId** | **Number**|  | 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## assignPatient

> Number assignPatient(patientId, caregiverId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let patientId = 789; // Number | 
let caregiverId = 789; // Number | 
apiInstance.assignPatient(patientId, caregiverId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patientId** | **Number**|  | 
 **caregiverId** | **Number**|  | 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## deleteCaregiver

> Number deleteCaregiver(caregiverId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let caregiverId = 789; // Number | 
apiInstance.deleteCaregiver(caregiverId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caregiverId** | **Number**|  | 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## deleteDrug

> Number deleteDrug(drugId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let drugId = 789; // Number | 
apiInstance.deleteDrug(drugId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **drugId** | **Number**|  | 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## deletePatient

> Number deletePatient(patientId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let patientId = 789; // Number | 
apiInstance.deletePatient(patientId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patientId** | **Number**|  | 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## dismissCaregiver

> Number dismissCaregiver(caregiverId, doctorId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let caregiverId = 789; // Number | 
let doctorId = 789; // Number | 
apiInstance.dismissCaregiver(caregiverId, doctorId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caregiverId** | **Number**|  | 
 **doctorId** | **Number**|  | 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## dismissCaregiverPatient

> Number dismissCaregiverPatient(patientId, caregiverId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let patientId = 789; // Number | 
let caregiverId = 789; // Number | 
apiInstance.dismissCaregiverPatient(patientId, caregiverId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patientId** | **Number**|  | 
 **caregiverId** | **Number**|  | 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## dismissPatient

> Number dismissPatient(patientId, doctorId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let patientId = 789; // Number | 
let doctorId = 789; // Number | 
apiInstance.dismissPatient(patientId, doctorId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patientId** | **Number**|  | 
 **doctorId** | **Number**|  | 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## findAllDoctors

> [Doctor] findAllDoctors()



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
apiInstance.findAllDoctors((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[Doctor]**](Doctor.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## getDoctorById

> Doctor getDoctorById(doctorId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let doctorId = 789; // Number | 
apiInstance.getDoctorById(doctorId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **doctorId** | **Number**|  | 

### Return type

[**Doctor**](Doctor.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## getHiredCaregivers

> [Caregiver] getHiredCaregivers(doctorId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let doctorId = 789; // Number | 
apiInstance.getHiredCaregivers(doctorId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **doctorId** | **Number**|  | 

### Return type

[**[Caregiver]**](Caregiver.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## hireCaregiver

> Number hireCaregiver(caregiverId, doctorId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let caregiverId = 789; // Number | 
let doctorId = 789; // Number | 
apiInstance.hireCaregiver(caregiverId, doctorId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caregiverId** | **Number**|  | 
 **doctorId** | **Number**|  | 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## insertNewDrug

> Number insertNewDrug(opts)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let opts = {
  'drug': new OpenApiDefinition.Drug() // Drug | 
};
apiInstance.insertNewDrug(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **drug** | [**Drug**](Drug.md)|  | [optional] 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*


## prescribeMedication

> Number prescribeMedication(patientId, medicationPlanId, opts)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let patientId = 789; // Number | 
let medicationPlanId = 789; // Number | 
let opts = {
  'prescribedMedication': new OpenApiDefinition.PrescribedMedication() // PrescribedMedication | 
};
apiInstance.prescribeMedication(patientId, medicationPlanId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patientId** | **Number**|  | 
 **medicationPlanId** | **Number**|  | 
 **prescribedMedication** | [**PrescribedMedication**](PrescribedMedication.md)|  | [optional] 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*


## undoMedication

> Number undoMedication(patientId, medicationPlanId, opts)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let patientId = 789; // Number | 
let medicationPlanId = 789; // Number | 
let opts = {
  'prescribedMedication': new OpenApiDefinition.PrescribedMedication() // PrescribedMedication | 
};
apiInstance.undoMedication(patientId, medicationPlanId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patientId** | **Number**|  | 
 **medicationPlanId** | **Number**|  | 
 **prescribedMedication** | [**PrescribedMedication**](PrescribedMedication.md)|  | [optional] 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*


## updateCaregiver

> User updateCaregiver(opts)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let opts = {
  'user': new OpenApiDefinition.User() // User | 
};
apiInstance.updateCaregiver(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | [**User**](User.md)|  | [optional] 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*


## updateDrug

> Number updateDrug(opts)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let opts = {
  'drug': new OpenApiDefinition.Drug() // Drug | 
};
apiInstance.updateDrug(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **drug** | [**Drug**](Drug.md)|  | [optional] 

### Return type

**Number**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*


## updatePatient

> User updatePatient(opts)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let opts = {
  'user': new OpenApiDefinition.User() // User | 
};
apiInstance.updatePatient(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | [**User**](User.md)|  | [optional] 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*


## viewTreatedPatients

> [Patient] viewTreatedPatients(doctorId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DoctorControllerApi();
let doctorId = 789; // Number | 
apiInstance.viewTreatedPatients(doctorId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **doctorId** | **Number**|  | 

### Return type

[**[Patient]**](Patient.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

