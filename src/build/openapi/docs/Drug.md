# OpenApiDefinition.Drug

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**drugId** | **Number** |  | [optional] 
**drugName** | **String** |  | [optional] 
**drugDescription** | **String** |  | [optional] 
**drugSideEffects** | **String** |  | [optional] 


