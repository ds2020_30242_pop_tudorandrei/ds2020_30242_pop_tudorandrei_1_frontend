# OpenApiDefinition.PrescribedMedication

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prescribedMedicationId** | **Number** |  | [optional] 
**dosage** | **Number** |  | [optional] 
**intakeMoment** | **String** |  | [optional] 
**drug** | [**Drug**](Drug.md) |  | [optional] 


