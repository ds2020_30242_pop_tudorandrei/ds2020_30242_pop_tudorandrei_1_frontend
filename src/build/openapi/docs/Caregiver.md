# OpenApiDefinition.Caregiver

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**caregiverId** | **Number** |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**doctor** | [**Doctor**](Doctor.md) |  | [optional] 
**patients** | [**[Patient]**](Patient.md) |  | [optional] 


