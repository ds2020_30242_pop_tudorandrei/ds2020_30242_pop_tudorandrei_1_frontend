# OpenApiDefinition.MedicationPlan

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**medicationPlanId** | **Number** |  | [optional] 
**startDate** | **Date** |  | [optional] 
**endDate** | **Date** |  | [optional] 
**prescribedMedications** | [**[PrescribedMedication]**](PrescribedMedication.md) |  | [optional] 


