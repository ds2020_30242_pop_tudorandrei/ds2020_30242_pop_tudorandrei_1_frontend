/**
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The LogInEntity model module.
 * @module model/LogInEntity
 * @version v0
 */
class LogInEntity {
    /**
     * Constructs a new <code>LogInEntity</code>.
     * @alias module:model/LogInEntity
     */
    constructor() { 
        
        LogInEntity.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>LogInEntity</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/LogInEntity} obj Optional instance to populate.
     * @return {module:model/LogInEntity} The populated <code>LogInEntity</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new LogInEntity();

            if (data.hasOwnProperty('username')) {
                obj['username'] = ApiClient.convertToType(data['username'], 'String');
            }
            if (data.hasOwnProperty('password')) {
                obj['password'] = ApiClient.convertToType(data['password'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} username
 */
LogInEntity.prototype['username'] = undefined;

/**
 * @member {String} password
 */
LogInEntity.prototype['password'] = undefined;






export default LogInEntity;

