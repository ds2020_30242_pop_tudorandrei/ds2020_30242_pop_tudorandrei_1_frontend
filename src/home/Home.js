import React from 'react'
import UserLogInForm from '../User/UserLogInForm';

import RegisterForm from '../User/RegisterForm';

import {Route, Switch, withRouter} from 'react-router-dom';
import DoctorFrontPage from '../doctor/doctorFrontPage';
import PatientFrontPage from '../patient/patientFrontPage';
import CaregiverFrontPage from '../caregiver/caregiverFrontPage';

//otherwise the callback won't change them 
var currentUserId = null;
var currentUserRole = null;
var isLoggedIn = null;

class Home extends React.Component {


    constructor(props){

        super(props);
    }

    whenLogIn(id, role) {
        isLoggedIn = true;

        currentUserId = id;
        currentUserRole = role;

        switch(role){
            case 'PATIENT' :this.props.history.push('/patient');
                            break;
            case 'DOCTOR' :this.props.history.push('/doctor');
                            break;
            case 'CAREGIVER' :this.props.history.push('/caregiver');
                            break;
            default :this.props.history.push('/'); break;
        }
        
      //  console.log("In LogIn func: " +isLoggedIn + " " + currentUserId +" "+ currentUserRole);
    };

    whenLogOut(){
        isLoggedIn = false;
        
        currentUserId = null;
        currentUserRole = null;
        window.localStorage.clear();
            
        console.log('Success');
    };

    whenLogInError() {
        console.log('Unkown user of incorrect password');
    };

    


    render() {
        //console.log("In render: "+isLoggedIn + " " + currentUserId +" "+ currentUserRole);
        return (
            <div >
                <Switch>

                    <Route
                        path='/'
                        exact
                        render = {() => <UserLogInForm whenLogIn={this.whenLogIn} whenLogInError={this.whenLogInError} />}
                    />

                    <Route
                        path='/login'
                        exact
                        render = {() => <UserLogInForm whenLogIn={this.whenLogIn} whenLogInError={this.whenLogInError} />}
                    />

                    <Route
                        path='/register'
                        exact
                        render = {() => <RegisterForm/>}
                    />

                    <Route
                        path='/patient'
                        exact
                        render = {() => <PatientFrontPage componentId={currentUserId} handleLogOut={this.whenLogOut}/>}
                    />

                    <Route
                        path='/doctor'
                        render = {() => <DoctorFrontPage componentId={currentUserId} handleLogOut={this.whenLogOut}/>}
                    />


                    <Route
                        path='/caregiver'
                        exact
                        render = {() => <CaregiverFrontPage componentId={currentUserId} handleLogOut={this.whenLogOut}/>}
                    />



                </Switch>
                
            </div>
        )
    };
}

export default withRouter(Home);
