import React from 'react'
import {withRouter} from 'react-router-dom';
import PatientControllerApi from '../build/openapi/src/api/PatientControllerApi';
import CaregiverControllerApi from '../build/openapi/src/api/CaregiverControllerApi';
import PatientTable from '../doctor/doctor-patient/PatientTable';
import CaregiverNavBar from './caregiverNavBar';
import PatientViewer from './patientViewer';
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import UserViewForm from '../User/userViewForm';

const patientControllerApi = new PatientControllerApi();
const caregiverControllerApi = new CaregiverControllerApi();

const patientColumns = [
    {
        Header: 'FirstName',
        accessor: 'user.userFirstName',
    },
    {
        Header: 'LastName',
        accessor: 'user.userLastName',
    },
    {
        Header: 'Username',
        accessor: 'user.userName',
    },
    {
        Header: 'MedicationPlans',
        accessor: 'viewPlansButton',
    }
];

class CaregiverFrontPage extends React.Component {

    
    constructor(props){
        
        super(props);
        this.caregiverId = props.componentId;
        this.toggleMedicine = this.toggleMedicine.bind(this);
        this.toggleInfo = this.toggleInfo.bind(this);

        this.state = {
            loggedIn : false,
            caregiver : JSON.parse(window.localStorage.getItem('user')),
            medicineSelected : false,
            userInfoSelected : false,
            currentUser : null,
            targetPatient : null,
            medicationPlans : [],
            patients:[],
            patientsLoaded:false,
            medicationPlansSelected : false,
        };
    }

    
    bindCaregiver(){

        if(this.state.caregiver === undefined || this.state.caregiver === null)
        {
            caregiverControllerApi.findById(this.caregiverId, (err, data, resp)=>{
                this.setState({caregiver : data, loggedIn: true});
                window.localStorage.setItem('user', JSON.stringify(data, this.getCircularReplacer()));
                window.localStorage.setItem('userId', JSON.stringify(this.state.caregiver.caregiverId));
            });
        }else{
            this.setState({loggedIn: true});
        }
    }

    bindPatients(){
        if(this.state.caregiver === undefined || this.state.caregiver === null)
        {
            caregiverControllerApi.findAssignedPatientsById(this.caregiverId, (err, data, resp)=>{
                this.setState({patients : data, patientsLoaded: true});
            });
        }
    }


    componentDidMount(){
       this.bindPatients();
       this.bindCaregiver();
    }

    //https://stackoverflow.com/questions/23593052/format-javascript-date-as-yyyy-mm-dd
    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [day, month, year].join('/');
    }

    //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cyclic_object_value
    getCircularReplacer = () => {
        const seen = new WeakSet();
        return (key, value) => {
          if (typeof value === "object" && value !== null) {
            if (seen.has(value)) {
              return;
            }
            seen.add(value);
          }
          return value;
        };
      };

    toggleMedicine(prescribedMedications){
        this.setState({medicineSelected : ! this.state.medicineSelected, currentMedicationList : prescribedMedications});
    }

    toggleInfo(user){
        this.setState({userInfoSelected : ! this.state.userInfoSelected, currentUser : user});
    }

    toggleMedicationPlans(patient){
        this.setState({medicationPlansSelected : ! this.state.medicationPlansSelected, targetPatient : patient});
    }

    render() {
        console.log(this.state.caregiver);

        var assignedPatients = []; 
        if(this.state.loggedIn && this.state.patientsLoaded){    
            assignedPatients = this.state.patients.slice(0);
            assignedPatients.forEach(patient => {
                patient.viewPlansButton = <Button onClick = {() => {this.toggleMedicationPlans(patient)}}>Medication Plans</Button>
            });
        }
        assignedPatients = assignedPatients.filter((patient)=>{return patient.patientId !== undefined});
        console.log("Pick a patient for the producer: ");
        console.log(assignedPatients);

        return (
        <div>
        {this.state.loggedIn && <CaregiverNavBar handleInfo = {()=>{this.toggleInfo(this.state.caregiver.user)}} handleLogOut= {this.props.handleLogOut}/>}

            <CardHeader>
                <strong> Medication Plans </strong>
            </CardHeader>
            <Card>
                <Row>
                    <Col sm={{size: '8', offset: 1}}>
                            {this.state.patientsLoaded && <PatientTable desiredColumns={patientColumns} tableData ={assignedPatients}/>} 
                    </Col>
                </Row>
            </Card>

            <Modal isOpen={this.state.medicineSelected} toggle={this.toggleMedicationPlans}
                    className={this.props.className} size="lg">
                <ModalHeader toggle={this.toggleMedicationPlans}> MedicationModal: </ModalHeader>
                <ModalBody>
                    <PatientViewer patientProp = {this.state.targetPatient}></PatientViewer>
                </ModalBody>
            </Modal>

            <Modal isOpen={this.state.userInfoSelected} toggle={this.toggleInfo}
                    className={this.props.className} size="lg">
                <ModalHeader toggle={this.toggleInfo}> UserDetails: </ModalHeader>
                <ModalBody>
                    <h1>UserDetails</h1>
                    <UserViewForm user = {this.state.currentUser}></UserViewForm>
                </ModalBody>
            </Modal>

        </div>

        )
    };
}

export default withRouter(CaregiverFrontPage);
