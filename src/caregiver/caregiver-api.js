import {HOST} from '../API/hosts';
import RestApiClient from "../API/rest-client";

const endpoint = {
    updateUser: '/doctor/caregiver/update'
};

function updateCaregiver(updatedUser, callback){
    let request = new Request(HOST.backend_api + endpoint.updateUser , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedUser)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export{
    updateCaregiver
};
